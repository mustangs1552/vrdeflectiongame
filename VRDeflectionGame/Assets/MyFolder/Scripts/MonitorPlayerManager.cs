﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonitorPlayerManager : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "MonitorPlayerManager";
    #endregion

    #region Static
    public static MonitorPlayerManager SINGLETON = null;
    #endregion

    #region Public
    [SerializeField]
    private GameObject playerPrefab = null;

    [Tooltip("The number of monitor players that will be playing.")]
    [Range(0, 4)]
    [SerializeField]
    private int numbOfPlayers = 0;
    #endregion

    #region Private
    //private int numbOfPlayers = 0;
    private List<GameObject> players = new List<GameObject>();
    private List<Transform> playerSpawnLoc = new List<Transform>();

    private MatchSettings matchSettingsObject = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Spawns all the players at thier own spawnpoints.
    public void SpawnAllPlayers()
    {
        foreach (GameObject player in players) SpawnPlayer(player);
    }

    // Spawns the chosen player at thier spawnpoint.
    public void SpawnPlayer(GameObject player)
    {
        for(int i = 0; i < players.Count; i++)
        {
            if(players[i] == player)
            {
                player.transform.position = playerSpawnLoc[i].position;
                player.transform.rotation = playerSpawnLoc[i].rotation;
                player.GetComponent<MonitorPlayer>().OnSpawn();
                return;
            }
        }
    }

    // Hide or show all players.
    public void ShowPlayers()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().BecomeVisible(false);
    }
    public void HidePlayers()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().BecomeInvisible();
    }

    // Informs each monitor player that the state has changed and to what state.
    public void TellPlayersStateChanged(AssassinModeState state)
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().StateChange(state);
    }
    public void TellPlayersStateChanged(AssaultState state)
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().StateChange(state);
    }

    public void PauseGame()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().Pause();
    }
    public void ResumeGame()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().Resume();
    }

    public float GetPlayerLookSensitivity(int player)
    {
        //if (player >= players.Count) return -1;
        //return players[player].GetComponent<MonitorPlayer>().LookSpeed;
        if (player == 0) return matchSettingsObject.P1LookSensitivity;
        else if (player == 1) return matchSettingsObject.P2LookSensitivity;
        else if (player == 2) return matchSettingsObject.P3LookSensitivity;
        else if (player == 3) return matchSettingsObject.P4LookSensitivity;
        else return -1;
    }
    public void SetPlayerLookSensitivity(int player, float newValue)
    {
        if (player >= players.Count) return;
        players[player].GetComponent<MonitorPlayer>().LookSpeed = newValue;
    }
    public void UpdatePlayerSensitivities()
    {
        float[] sensitivities = { GetPlayerLookSensitivity(0), GetPlayerLookSensitivity(1), GetPlayerLookSensitivity(2), GetPlayerLookSensitivity(3) };
        matchSettingsObject.SetSensitivitySettings(sensitivities);
    }

    public void Win()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().Win();
    }
    public void Lose()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().Lose();
    }
    #endregion

    #region Private
    // For each player that is playing, spawns a character controller in and sets up the camera and device input to work with thier controller.
    // Sets up the cameras to run in split-screen mode.
    private void SetUpPlayers()
    {
        for (int i = 0; i < numbOfPlayers; i++)
        {
            GameObject player = Instantiate(playerPrefab);
            player.transform.position = transform.position;
            players.Add(player);
        }
        PrintDebugMsg("Number of players created: " + players.Count + " (" + numbOfPlayers + ")");
        
        Rect[] viewportRects = new Rect[4];
        if (players.Count == 1)
        {
            viewportRects[0] = new Rect(0, 0, 1, 1);
            viewportRects[1] = new Rect(0, 0, 0, 0);
            viewportRects[2] = new Rect(0, 0, 0, 0);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 2)
        {
            viewportRects[0] = new Rect(0, .5f, 1, 1);
            viewportRects[1] = new Rect(0, 0, 1, .5f);
            viewportRects[2] = new Rect(0, 0, 0, 0);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 3)
        {
            viewportRects[0] = new Rect(0, .5f, 1, 1);
            viewportRects[1] = new Rect(0, 0, .5f, .5f);
            viewportRects[2] = new Rect(.5f, 0, 1, .5f);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 4)
        {
            viewportRects[0] = new Rect(0, .5f, .5f, 1);
            viewportRects[1] = new Rect(.5f, .5f, 1, 1);
            viewportRects[2] = new Rect(0, 0, .5f, .5f);
            viewportRects[3] = new Rect(.5f, 0, 1, .5f);
        }
        for (int i = 0; i < players.Count; i++)
        {
            MonitorPlayer mpScript = players[i].GetComponent<MonitorPlayer>();
            mpScript.Cam.rect = viewportRects[i];
            mpScript.PlayerNum = i + 1;
        }

        SetUpMatchSettings();
    }

    // Finds all the available spawnpoints for the monitor players.
    private void FindPlayerSpawns()
    {
        GameObject[] foundSpawns = GameObject.FindGameObjectsWithTag("Spawnpoint");
        foreach (GameObject obj in foundSpawns) playerSpawnLoc.Add(obj.transform);

        PrintDebugMsg("Monitor player spawnpoints found: " + playerSpawnLoc.Count);
    }

    private void SetUpMatchSettings()
    {
        matchSettingsObject = GameObject.Find("MatchSettings").GetComponent<MatchSettings>();
        if (matchSettingsObject == null) PrintErrorDebugMsg("No match settings found!");

        SetPlayerLookSensitivity(0, matchSettingsObject.P1LookSensitivity);
        SetPlayerLookSensitivity(1, matchSettingsObject.P2LookSensitivity);
        SetPlayerLookSensitivity(2, matchSettingsObject.P3LookSensitivity);
        SetPlayerLookSensitivity(3, matchSettingsObject.P4LookSensitivity);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public int NumbOfPlayers
    {
        get
        {
            return numbOfPlayers;
        }
        set
        {
            numbOfPlayers = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (MonitorPlayerManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one MonitorPlayerManager singletons detected!");

        SetUpPlayers();
        FindPlayerSpawns();
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}