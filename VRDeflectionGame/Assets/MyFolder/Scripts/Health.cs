﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Team
{
    VRPlayer,
    MonitorPlayer,
    None
}

public class Health : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Health";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    [Tooltip ("Is this the script that holds the health variable that this character uses?")]
    private bool isMainHealthObj = true;
    [SerializeField]
    [Tooltip("If \"isMainHealthObj\" is set to true, what object holds the main health variable? Ignore if \"isMainHealthObj\" is false.")]
    private Health mainHealthObj = null;

    [SerializeField]
    private float health = 100;
    [SerializeField]
    private Team affiliation = Team.None;

    [SerializeField]
    private Slider healthBar = null;
    #endregion

    #region Private
    private float currHealth = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
     // If this is where the main health variable is then adjust this one.
    // If not then tell the object that holds the main health variable to adjust it.
    public void AdjustHealth(float amount)
    {
        if (isMainHealthObj)
        {
            if (amount < 0) PrintDebugMsg("Took " + Mathf.Abs(amount) + " damage!");
            else PrintDebugMsg("healed for " + amount + " health!");

            currHealth += amount;
            PrintDebugMsg("New health: " + currHealth);

            CheckHealthStatus();
        }
        else
        {
            PrintDebugMsg("Sending amount to main health object...");
            mainHealthObj.AdjustHealth(amount);
        }
    }
    #endregion

    #region Private
    // Checks to see if health was depleted to 0 or if it exceded the maximum health.
    private void CheckHealthStatus()
    {
        if(currHealth <= 0) Die();
        if (currHealth > health) currHealth = health;
    }

    // Find out the top-most parent of this object and destroy it.
    private void Die()
    {
        Transform mostParent = transform;
        while (mostParent.parent != null) mostParent = mostParent.parent;

        AssassinModeGameController.SINGLETON.PlayerDied(this, mostParent);

        SendMessageUpwards("OnDeath", SendMessageOptions.DontRequireReceiver);
    }

    private void UpdateHealthBar()
    {
        if(healthBar != null)
        {
            healthBar.maxValue = health;
            healthBar.value = currHealth;
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Team Affiliation
    {
        get
        {
            return affiliation;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    private void OnTriggerEnter(Collider collider)
    {
        PrintDebugMsg("Collided with " + collider.gameObject.name + "!");

        if(collider.gameObject.GetComponent<Laser>() != null && collider.gameObject.GetComponent<Laser>().Affiliation != affiliation)
        {
            PrintDebugMsg("Was attacked by " + collider.gameObject.GetComponent<Laser>().Affiliation + "!");
            AdjustHealth(collider.gameObject.GetComponent<Laser>().Damage);
            Destroy(collider.gameObject);
        }
    }
    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        currHealth = health;

        if (!isMainHealthObj) affiliation = mainHealthObj.affiliation;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        UpdateHealthBar();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}