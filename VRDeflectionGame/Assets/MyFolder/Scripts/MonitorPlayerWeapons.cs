﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum Weapons
{
    Pistol,
    AssaultRifle,
    SMG,
    MissileLauncher,
    None
}
public enum WeaponAction
{
    Start,
    Continuing,
    Stop,
    None
}

public class MonitorPlayerWeapons : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "MonitorPlayerWeapons";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private Transform mainHealthObj = null;
    [SerializeField]
    private Slider chargeSlider = null;
    [SerializeField]
    private Text missileCounter = null;
    #endregion

    #region Private
    private int activeWeapon = 0; // 0 = none; 1 = pistol; 2 = AR; 3 = SMG; 4 = ML

    private SemiAutoGun pistol = null;
    private FullAutoGun assaultRifle = null;
    private FullAutoGun smg = null;
    private OneShotReloadGun missileLauncher = null;

    private List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
     // Selects the weapon in the slot that is passed as a Vector2 direction (ie: Vector2.up, .down, ...) for the directional buttons on a controller.
    // A selection value thats not of the 4 main directions will be no active weapon.
    public void SelectWeapon(Vector2 selection)
    {
        if (selection == Vector2.up) activeWeapon = 1;
        else if (selection == Vector2.right && assaultRifle.Shooter != null) activeWeapon = 2;
        else if (selection == Vector2.down && smg.Shooter != null) activeWeapon = 3;
        else if (selection == Vector2.left && missileLauncher.Shooter != null) activeWeapon = 4;

        ToggleWeapons();
        PrintDebugMsg("activeWeapon: " + activeWeapon);
    }

    // Fires the weapon that is currently active (the one the player has selected).
    public void FireWeapon(WeaponAction action)
    {
        PrintDebugMsg("Firing weapon...");

        bool shot = false;
        switch(activeWeapon)
        {
            case 1:
                if(action == WeaponAction.Start) pistol.Fire();
                shot = true;
                break;
            case 2:
                assaultRifle.Fire();
                shot = true;
                break;
            case 3:
                smg.Fire();
                shot = true;
                break;
            case 4:
                if(action == WeaponAction.Start) missileLauncher.Fire();
                shot = true;
                break;
        }

        if(shot) GetComponent<MonitorPlayer>().BecomeVisible();
    }

    // Re-supplies the weapon that the player is currently holding. Returns true if successfully re-supplied a weapon.
    public bool ResupplyWeapon(Ammo ammo)
    {
        PrintDebugMsg("Re-suppling a weapon...");

        if(ammo.Type == AmmoType.Charge)
        {
            PrintDebugMsg("...a charge weapon...");
            switch(activeWeapon)
            {
                case 1:
                    PrintDebugMsg("...the pistol.");
                    return pistol.Resupply(ammo.Amount * 100);
                case 2:
                    PrintDebugMsg("...the assault rifle.");
                    return assaultRifle.Resupply(ammo.Amount * 100);
                case 3:
                    PrintDebugMsg("...the SMG.");
                    return smg.Resupply(ammo.Amount * 100);
            }
        }
        else if(ammo.Type == AmmoType.Missle)
        {
            PrintDebugMsg("...the missile launcher.");
            return missileLauncher.Resupply((int)ammo.Amount);
        }

        return false;
    }

    // Activate the picked-up weapon that the player object has.
    public bool PickupWeapon(Transform weapon)
    {
        if (weapon.GetComponent<FullAutoGun>() != null)
        {
            if(assaultRifle.Shooter == null && weapon.GetComponent<FullAutoGun>().Type == Weapons.AssaultRifle)
            {
                PrintDebugMsg("Picked up an Assault Rifle.");
                assaultRifle.OnPickUp(mainHealthObj);
                activeWeapon = 2;

                ToggleWeapons();
                return true;
            }
            else if (smg.Shooter == null && weapon.GetComponent<FullAutoGun>().Type == Weapons.SMG)
            {
                PrintDebugMsg("Picked up a SMG.");
                smg.OnPickUp(mainHealthObj);
                activeWeapon = 3;

                ToggleWeapons();
                return true;
            }
        }
        else if (weapon.GetComponent<OneShotReloadGun>() != null && missileLauncher.Shooter == null && weapon.GetComponent<OneShotReloadGun>().Type == Weapons.MissileLauncher)
        {
            PrintDebugMsg("Picked up a Missile Launcher.");
            missileLauncher.OnPickUp(mainHealthObj);
            activeWeapon = 4;

            ToggleWeapons();
            return true;
        }

        return false;
    }
    #endregion

    #region Private
    // Looks for all the weapons as children on this player and assigns thier scripts to thier variables.
    private void SetUpWeapons()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            for (int ii = 0; ii < currChild.transform.childCount; ii++)
            {
                if (currChild.transform.GetChild(ii).GetComponent<MeshRenderer>() != null) meshRenderers.Add(currChild.transform.GetChild(ii).GetComponent<MeshRenderer>());

                Transform nextCurrChild = currChild.transform.GetChild(ii);
                for (int iii = 0; iii < nextCurrChild.childCount; iii++)
                {
                    if (nextCurrChild.transform.GetChild(iii).GetComponent<MeshRenderer>() != null) meshRenderers.Add(nextCurrChild.transform.GetChild(iii).GetComponent<MeshRenderer>());
                }
            }

            if (currChild.name == "Pistol") pistol = currChild.GetComponent<SemiAutoGun>();
            if (currChild.name == "AssaultRifle") assaultRifle = currChild.GetComponent<FullAutoGun>();
            if (currChild.name == "SMG") smg = currChild.GetComponent<FullAutoGun>();
            if (currChild.name == "MissileLauncher") missileLauncher = currChild.GetComponent<OneShotReloadGun>();
        }

        Transform camTransform = GetComponent<MonitorPlayer>().Cam.transform;
        pistol.transform.parent = camTransform;
        assaultRifle.transform.parent = camTransform;
        smg.transform.parent = camTransform;
        missileLauncher.transform.parent = camTransform;

        pistol.OnPickUp(mainHealthObj);
        activeWeapon = 1;
        assaultRifle.gameObject.SetActive(false);
        smg.gameObject.SetActive(false);
        missileLauncher.gameObject.SetActive(false);

        GetComponent<MonitorPlayer>().SetRenderers(meshRenderers);
    }

    // Enables the weapon that activeWeapon currently resembles and disables the rest.
    private void ToggleWeapons()
    {
        pistol.gameObject.SetActive(false);
        assaultRifle.gameObject.SetActive(false);
        smg.gameObject.SetActive(false);
        missileLauncher.gameObject.SetActive(false);

        chargeSlider.gameObject.SetActive(false);
        missileCounter.gameObject.SetActive(false);

        if (activeWeapon == 1)
        {
            pistol.gameObject.SetActive(true);
            chargeSlider.gameObject.SetActive(true);
        }
        else if (activeWeapon == 2 && assaultRifle.Shooter != null)
        {
            assaultRifle.gameObject.SetActive(true);
            chargeSlider.gameObject.SetActive(true);
        }
        else if (activeWeapon == 3 && smg.Shooter != null)
        {
            smg.gameObject.SetActive(true);
            chargeSlider.gameObject.SetActive(true);
        }
        else if (activeWeapon == 4 && missileLauncher.Shooter != null)
        {
            missileLauncher.gameObject.SetActive(true);
            missileCounter.gameObject.SetActive(true);
        }
    }

    // Update the UI for each of the ammo types when those weapons are active.
    private void UpdateAmmoUI()
    {
        switch(activeWeapon)
        {
            case 1:
                missileCounter.gameObject.SetActive(false);
                chargeSlider.gameObject.SetActive(true);
                chargeSlider.value = pistol.CurrCharge;
                break;
            case 2:
                missileCounter.gameObject.SetActive(false);
                if (assaultRifle.Shooter != null)
                {
                    chargeSlider.gameObject.SetActive(true);
                    chargeSlider.value = assaultRifle.CurrCharge;
                }
                else chargeSlider.gameObject.SetActive(false);
                break;
            case 3:
                missileCounter.gameObject.SetActive(false);
                if (smg.Shooter != null)
                {
                    chargeSlider.gameObject.SetActive(true);
                    chargeSlider.value = smg.CurrCharge;
                }
                else chargeSlider.gameObject.SetActive(false);
                break;
            case 4:
                if (missileLauncher.Shooter != null)
                {
                    missileCounter.gameObject.SetActive(true);
                    missileCounter.text = missileLauncher.CurrAmmo + " / " + missileLauncher.MaxAmmo;
                }
                break;
        }
    }

    private void OnDeath()
    {
        pistol.ResetCharge();
        assaultRifle.OnDrop();
        smg.OnDrop();
        missileLauncher.OnDrop();

        SelectWeapon(Vector2.up);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public List<MeshRenderer> MeshRenderers
    {
        get
        {
            return meshRenderers;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        SetUpWeapons();
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        UpdateAmmoUI();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}