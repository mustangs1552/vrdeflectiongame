﻿// Handles the position and rotation of the hands using the Touch controllers as well as other actions like general buttons.
using UnityEngine;
using System.Collections;

public class TouchControllers : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "TouchControllers";
    #endregion

    #region Static

    #endregion

    #region Public

    #endregion

    #region Private
    private Transform leftHand = null;
    private Transform rightHand = null;
    private bool isPaused = false;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public

    #endregion

    #region Private
    // Sets up the objects. Goes through children and sets them to thier proper variables.
    private void SetUpObjects()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "LeftHand") leftHand = currChild;
            else if (currChild.name == "RightHand") rightHand = currChild;
        }
    }

    // Check for input from the controllers.
    private void CheckInput()
    {
        if (!isPaused)
        {
            AssassinModeState assassinModeState = (AssassinModeGameController.SINGLETON != null) ? AssassinModeGameController.SINGLETON.State : AssassinModeState.None;
            AssaultState assaultModeState = (AssaultModeGameController.SINGLETON != null) ? AssaultModeGameController.SINGLETON.State : AssaultState.None;

            if ((assassinModeState == AssassinModeState.PlayerInput || assassinModeState == AssassinModeState.EndGame) || (assaultModeState == AssaultState.PlayerInput || assaultModeState == AssaultState.EndGame))
            {
                if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Touch))
                {
                    if(AssassinModeGameController.SINGLETON != null) AssassinModeGameController.SINGLETON.PlayerReady(Team.VRPlayer);
                    if (AssaultModeGameController.SINGLETON != null) AssaultModeGameController.SINGLETON.PlayerReady(Team.VRPlayer);
                }
            }

            if (OVRInput.GetUp(OVRInput.Button.Start, OVRInput.Controller.Touch)) AssassinModeGameController.SINGLETON.PauseGame();
        }
        else
        {
            if (OVRInput.GetUp(OVRInput.Button.Start, OVRInput.Controller.Touch)) AssassinModeGameController.SINGLETON.ResumeGame();
            if (OVRInput.GetUp(OVRInput.Button.Two, OVRInput.Controller.Touch)) AssassinModeGameController.SINGLETON.LoadMainMenu();
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Transform LeftHand
    {
        get
        {
            return leftHand;
        }
    }
    public Transform RightHand
    {
        get
        {
            return rightHand;
        }
    }
    public bool IsPaused
    {
        set
        {
            isPaused = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        SetUpObjects();
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        leftHand.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        leftHand.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);
        rightHand.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
        rightHand.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch);

        CheckInput();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}