﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class VRPlayerHolster : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "VRPlayerHolster";
    #endregion

    #region Static

    #endregion

    #region Public
    
    #endregion

    #region Private
    private Transform heldObj = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    public void InsertObj(Transform obj)
    {
        PrintDebugMsg("Inserting " + obj.name + "...");
        heldObj = obj;
        heldObj.GetComponent<Rigidbody>().isKinematic = true;
        heldObj.parent = transform;

        for (int i = 0; i < heldObj.childCount; i++)
        {
            if (heldObj.GetChild(i).gameObject.name == "GrabbingMount")
            {
                PrintDebugMsg("Found a child named \"GrabbingMount\" to serve as a mounting point!");

                heldObj.rotation = transform.rotation;
                heldObj.rotation = heldObj.GetChild(i).rotation;

                heldObj.position = transform.position;
                heldObj.Translate(-heldObj.GetChild(i).localPosition);

                break;
            }
        }
    }

    public Transform TakeObj()
    {
        heldObj.parent = null;
        heldObj.GetComponent<Rigidbody>().isKinematic = false;
        Transform obj = heldObj;
        heldObj = null;
        return obj;
    }
    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Transform HeldObj
    {
        get
        {
            return heldObj;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    
    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}