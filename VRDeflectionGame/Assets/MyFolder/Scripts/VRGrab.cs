﻿ // Handles grabbing and throwing objects.
// Objects that use the GrabbingMount must be of scale (1, 1, 1) in order for the posiitoning to work poperly.
using UnityEngine;
using System.Collections;

public class VRGrab : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "VRGrab";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private Transform mainHealthObj = null;

    [SerializeField]
    private float dropDelay = .5f;
    [SerializeField]
    private float triggerHold = .5f;

    [SerializeField]
    private VRPlayerHolster holsterOne = null;
    [SerializeField]
    private VRPlayerHolster holsterTwo = null;
    #endregion

    #region Private
    private Transform leftHand = null;
    private Transform rightHand = null;

    private Transform grabbedObjLeft = null;
    private Transform grabbedObjRight = null;

    private Vector3 lastValidVelocityRight = Vector3.zero;
    private Vector3 lastValidVelocityLeft = Vector3.zero;
    private Vector3 lastValidAngularVelocityRight = Vector3.zero;
    private Vector3 lastValidAngularVelocityLeft = Vector3.zero;

    private float dropStartLeft = 0;
    private float dropStartRight = 0;

    private bool triggerPressedLeft = false;
    private float startTriggerPressedLeft = 0;
    private bool activatedLeft = false;
    private bool triggerPressedRight = false;
    private float startTriggerPressedRight = 0;
    private bool activatedRight = false;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    public Transform InsertObj(string handType)
    {
        if(handType == "Left")
        {
            PrintDebugMsg("Inserting the left obj...");

            return grabbedObjLeft;
        }
        else if (handType == "Right")
        {
            PrintDebugMsg("Inserting the right obj...");

            return grabbedObjRight;
        }

        return null;
    }
    #endregion

    #region Private
    // The grab and drop functions.
    // handType is the hand that is grabbing/dropping.
    private void GrabObj(string handType)
    {
        PrintDebugMsg("Attempting to grab with \"" + handType + "\" hand...");

        RaycastHit[] hits;
        Transform closestObj = null;
        float closestDist = -1;

        if (handType == "Right")
        {
            hits = Physics.SphereCastAll(new Ray(rightHand.position, Vector3.forward), .1f, .1f);
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.tag != "Grabable") continue;
                if (grabbedObjLeft == hit.transform) continue;
                float dist = Vector3.Distance(transform.position, hit.transform.position);
                PrintDebugMsg("Checking " + hit.transform.gameObject.name + " (" + dist + ")...");

                if (dist <= closestDist || closestDist == -1)
                {
                    closestObj = hit.transform;
                    closestDist = dist;
                    PrintDebugMsg(closestObj.name + " is new closest object.");
                }
            }

            if (closestObj != null)
            {
                if (closestObj == holsterOne.HeldObj)
                {
                    holsterOne.TakeObj();
                    PrintDebugMsg("Insterted in a holster.");
                }
                else if (closestObj == holsterTwo.HeldObj)
                {
                    holsterTwo.TakeObj();
                    PrintDebugMsg("Insterted in a holster.");
                }

                grabbedObjRight = closestObj;
                grabbedObjRight.parent = rightHand;
                grabbedObjRight.GetComponent<Rigidbody>().isKinematic = true;

                for (int i = 0; i < grabbedObjRight.childCount; i++)
                {
                    if (grabbedObjRight.GetChild(i).gameObject.name == "GrabbingMount")
                    {
                        PrintDebugMsg("Found a child named \"GrabbingMount\" to serve as a mounting point!");

                        grabbedObjRight.rotation = rightHand.rotation;
                        grabbedObjRight.rotation = grabbedObjRight.GetChild(i).rotation;

                        grabbedObjRight.position = rightHand.position;
                        grabbedObjRight.Translate(-grabbedObjRight.GetChild(i).localPosition);

                        break;
                    }
                }

                if (grabbedObjRight.GetComponent<PlasmaSword>() != null) grabbedObjRight.GetComponent<PlasmaSword>().Holder = mainHealthObj;
                PrintDebugMsg("Grabbing " + grabbedObjRight.gameObject.name + "...");
            }
        }
        else if (handType == "Left")
        {
            hits = Physics.SphereCastAll(new Ray(leftHand.position, Vector3.forward), .1f, .1f);
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.tag != "Grabable") continue;
                if (grabbedObjRight == hit.transform) continue;
                float dist = Vector3.Distance(transform.position, hit.transform.position);
                PrintDebugMsg("Checking " + hit.transform.gameObject.name + " (" + dist + ")...");

                if (dist <= closestDist || closestDist == -1)
                {
                    closestObj = hit.transform;
                    closestDist = dist;
                    PrintDebugMsg(closestObj.name + " is new closest object.");
                }
            }

            if (closestObj != null)
            {
                if (closestObj == holsterOne.HeldObj)
                {
                    holsterOne.TakeObj();
                    PrintDebugMsg("Insterted in a holster.");
                }
                else if (closestObj == holsterTwo.HeldObj)
                {
                    holsterTwo.TakeObj();
                    PrintDebugMsg("Insterted in a holster.");
                }

                grabbedObjLeft = closestObj;
                grabbedObjLeft.parent = leftHand;
                grabbedObjLeft.GetComponent<Rigidbody>().isKinematic = true;

                for (int i = 0; i < grabbedObjLeft.childCount; i++)
                {
                    if (grabbedObjLeft.GetChild(i).gameObject.name == "GrabbingMount")
                    {
                        PrintDebugMsg("Found a child named \"GrabbingMount\" to serve as a mounting point!");

                        grabbedObjLeft.rotation = leftHand.rotation;
                        grabbedObjLeft.rotation = grabbedObjLeft.GetChild(i).rotation;

                        grabbedObjLeft.position = leftHand.position;
                        grabbedObjLeft.Translate(-grabbedObjLeft.GetChild(i).localPosition);

                        break;
                    }
                }

                if (grabbedObjLeft.GetComponent<PlasmaSword>() != null) grabbedObjLeft.GetComponent<PlasmaSword>().Holder = mainHealthObj;
                PrintDebugMsg("Grabbing " + grabbedObjLeft.gameObject.name + "...");
            }
        }
    }
    private void DropObj(string handType)
    {
        PrintDebugMsg("Droping/throwing with \"" + handType + "\" hand...");

        if (handType == "Right")
        {
            grabbedObjRight.parent = null;
            grabbedObjRight.GetComponent<Rigidbody>().isKinematic = false;

            if (OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch) == Vector3.zero) grabbedObjRight.GetComponent<Rigidbody>().velocity = lastValidVelocityRight;
            else grabbedObjRight.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);

            if (OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch) == Vector3.zero) grabbedObjRight.GetComponent<Rigidbody>().angularVelocity = lastValidAngularVelocityRight;
            else grabbedObjRight.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);

            RaycastHit[] hits = Physics.SphereCastAll(new Ray(rightHand.position, Vector3.forward), .1f, .1f);
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.GetComponent<VRPlayerHolster>() == holsterOne && holsterOne.HeldObj == null) holsterOne.InsertObj(grabbedObjRight);
                else if (hit.transform.GetComponent<VRPlayerHolster>() == holsterTwo && holsterTwo.HeldObj == null) holsterTwo.InsertObj(grabbedObjRight);
            }

            grabbedObjRight.SendMessage("OnDropped");

            lastValidVelocityRight = Vector3.zero;
            lastValidAngularVelocityRight = Vector3.zero;
            grabbedObjRight = null;
        }
        else if (handType == "Left")
        {
            grabbedObjLeft.parent = null;
            grabbedObjLeft.GetComponent<Rigidbody>().isKinematic = false;

            if (OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch) == Vector3.zero) grabbedObjLeft.GetComponent<Rigidbody>().velocity = lastValidVelocityLeft;
            else grabbedObjLeft.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);

            if (OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch) == Vector3.zero) grabbedObjLeft.GetComponent<Rigidbody>().angularVelocity = lastValidAngularVelocityLeft;
            else grabbedObjLeft.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch);

            RaycastHit[] hits = Physics.SphereCastAll(new Ray(leftHand.position, Vector3.forward), .1f, .1f);
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.GetComponent<VRPlayerHolster>() == holsterOne && holsterOne.HeldObj == null) holsterOne.InsertObj(grabbedObjLeft);
                else if (hit.transform.GetComponent<VRPlayerHolster>() == holsterTwo && holsterTwo.HeldObj == null) holsterTwo.InsertObj(grabbedObjLeft);
            }

            grabbedObjLeft.SendMessage("OnDropped");

            lastValidVelocityLeft = Vector3.zero;
            lastValidAngularVelocityLeft = Vector3.zero;
            grabbedObjLeft = null;
        }
    }

    // Check the input of the Touch controllers and determine if anything needs to be done.
    private void CheckInput()
    {
        if (grabbedObjLeft == null && OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) >= .9f) GrabObj("Left");
        if (grabbedObjRight == null && OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) >= .9f) GrabObj("Right");

        if (grabbedObjLeft != null)
        {
            if (OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch) != Vector3.zero) lastValidVelocityLeft = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
            if (OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch) != Vector3.zero) lastValidAngularVelocityLeft = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch);

            if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) == 0)
            {
                if (dropStartLeft == 0) dropStartLeft = Time.time;
                else if (Time.time - dropStartLeft >= dropDelay)
                {
                    DropObj("Left");
                    dropStartLeft = 0;
                }
            }

            if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger) >= .19)
            {
                if (!triggerPressedLeft)
                {
                    triggerPressedLeft = true;
                    startTriggerPressedLeft = Time.time;
                }
                else if (Time.time - startTriggerPressedLeft >= triggerHold & !activatedLeft)
                {
                    if (grabbedObjLeft.GetComponent<PlasmaSword>() != null)
                    {
                        //grabbedObjLeft.GetComponent<PlasmaSword>().ActivateDeactivate();
                        activatedLeft = true;
                    }
                }
            }
            else if (triggerPressedLeft)
            {
                triggerPressedLeft = false;
                activatedLeft = false;
            }
        }
        if (grabbedObjRight != null)
        {
            if (OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch) != Vector3.zero) lastValidVelocityRight = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
            if (OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch) != Vector3.zero) lastValidAngularVelocityRight = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);

            if (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) == 0)
            {
                if (dropStartRight == 0) dropStartRight = Time.time;
                else if (Time.time - dropStartRight >= dropDelay)
                {
                    DropObj("Right");
                    dropStartRight = 0;
                }
            }

            if (OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) >= .19)
            {
                if (!triggerPressedRight)
                {
                    triggerPressedRight = true;
                    startTriggerPressedRight = Time.time;
                }
                else if (Time.time - startTriggerPressedRight >= triggerHold & !activatedRight)
                {
                    if (grabbedObjRight.GetComponent<PlasmaSword>() != null)
                    {
                        //grabbedObjRight.GetComponent<PlasmaSword>().ActivateDeactivate();
                        activatedRight = true;
                    }
                }
            }
            else if (triggerPressedRight)
            {
                triggerPressedRight = false;
                activatedRight = false;
            }
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Transform LeftHand
    {
        get
        {
            return leftHand;
        }
    }
    public Transform RightHand
    {
        get
        {
            return rightHand;
        }
    }

    public Transform GrabbedObjLeft
    {
        get
        {
            return grabbedObjLeft;
        }
    }
    public Transform GrabbedObjRight
    {
        get
        {
            return grabbedObjRight;
        }
    }

    public VRPlayerHolster HolsterOne
    {
        get
        {
            return holsterOne;
        }
    }
    public VRPlayerHolster HolsterTwo
    {
        get
        {
            return holsterTwo;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        leftHand = GetComponent<TouchControllers>().LeftHand;
        rightHand = GetComponent<TouchControllers>().RightHand;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        CheckInput();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}