﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class MatchSettings : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "MatchSettings";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private float defaultSensitivity = 2.5f;
    #endregion

    #region Private
    private int numbOfPlayers = 0;
    private float timeLimit = 0;

    private float p1LookSensitivity = 0;
    private float p2LookSensitivity = 0;
    private float p3LookSensitivity = 0;
    private float p4LookSensitivity = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    public void SetSettings(int players, float time)
    {
        numbOfPlayers = players;
        timeLimit = time;
    }

    public void SetSensitivitySettings(float[] sensitivities)
    {
        p1LookSensitivity = sensitivities[0];
        PlayerPrefs.SetFloat("P1Sensitivity", sensitivities[0]);

        p2LookSensitivity = sensitivities[1];
        PlayerPrefs.SetFloat("P2Sensitivity", sensitivities[1]);

        p3LookSensitivity = sensitivities[2];
        PlayerPrefs.SetFloat("P3Sensitivity", sensitivities[2]);

        p4LookSensitivity = sensitivities[3];
        PlayerPrefs.SetFloat("P4Sensitivity", sensitivities[3]);
    }
    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public int NumbOfPlayers
    {
        get
        {
            return numbOfPlayers;
        }
    }
    public float TimeLimit
    {
        get
        {
            return timeLimit;
        }
    }
    public float P1LookSensitivity
    {
        get
        {
            return p1LookSensitivity;
        }
    }
    public float P2LookSensitivity
    {
        get
        {
            return p2LookSensitivity;
        }
    }
    public float P3LookSensitivity
    {
        get
        {
            return p3LookSensitivity;
        }
    }
    public float P4LookSensitivity
    {
        get
        {
            return p4LookSensitivity;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        p1LookSensitivity = (PlayerPrefs.GetFloat("P1Sensitivity") >= 1) ? PlayerPrefs.GetFloat("P1Sensitivity") : defaultSensitivity;
        p2LookSensitivity = (PlayerPrefs.GetFloat("P2Sensitivity") >= 1) ? PlayerPrefs.GetFloat("P2Sensitivity") : defaultSensitivity;
        p3LookSensitivity = (PlayerPrefs.GetFloat("P3Sensitivity") >= 1) ? PlayerPrefs.GetFloat("P3Sensitivity") : defaultSensitivity;
        p4LookSensitivity = (PlayerPrefs.GetFloat("P4Sensitivity") >= 1) ? PlayerPrefs.GetFloat("P4Sensitivity") : defaultSensitivity;
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {

    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}