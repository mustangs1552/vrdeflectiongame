﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class OneShotReloadGun : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "OneShotReloadGun";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private Weapons type = Weapons.None;
    [SerializeField]
    private GameObject projectile = null;
    [SerializeField]
    private float cooldown = 2;
    [SerializeField]
    private int maxAmmo = 3;

    [SerializeField]
    private AudioClip shotSound = null;
    #endregion

    #region Private
    private int currAmmo = 0;
    private float lastShot = 0;
    private Transform projSpawnPoint = null;
    private Transform shooter = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Fire the projectile and set its position and rotation to match the spawn point. Also set the affiliation and and parent it to the anchor.
    public void Fire()
    {
        if (Time.time - lastShot >= cooldown && currAmmo > 0)
        {
            Transform missile = Instantiate(projectile, WeaponManager.MONITORPLAYER_PROJANCHOR).transform;
            missile.position = projSpawnPoint.position;
            missile.rotation = projSpawnPoint.rotation;
            missile.GetComponent<Missle>().Affiliation = Team.MonitorPlayer;

            GetComponent<AudioSource>().clip = shotSound;
            GetComponent<AudioSource>().Play();

            currAmmo--;
            lastShot = Time.time;
        }
    }

    // This object was just picked up by someone.
    public void OnPickUp(Transform pickedUpBy)
    {
        shooter = pickedUpBy;
        currAmmo = maxAmmo;
    }
    public void OnDrop()
    {
        shooter = null;
    }

    // Re-supply this weapon's ammo type.
    public bool Resupply(int amount)
    {
        PrintDebugMsg("Ammo: " + currAmmo + "/" + maxAmmo);
        if (currAmmo < maxAmmo)
        {
            currAmmo += amount;
            if (currAmmo > maxAmmo) currAmmo = maxAmmo;
            return true;
        }

        return false;
    }
    #endregion

    #region Private
    // Sets up all the variables that need objects assigned.
    private void SetUpObjs()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "ProjSpawnPoint") projSpawnPoint = currChild;
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Transform Shooter
    {
        get
        {
            return shooter;
        }
    }
    public float CurrAmmo
    {
        get
        {
            return currAmmo;
        }
    }
    public float MaxAmmo
    {
        get
        {
            return maxAmmo;
        }
    }
    public Weapons Type
    {
        get
        {
            return type;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        SetUpObjs();

        lastShot = -cooldown;
        currAmmo = maxAmmo;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}