﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PausedOptions : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "PausedOptions";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private GameObject mainPausedPanel = null;
    [SerializeField]
    private GameObject optionsPanel = null;

    [SerializeField]
    private Slider p1Slider, p2Slider, p3Slider, p4Slider = null;
    #endregion

    #region Private
    private MatchSettings matchSettingsObject = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    public void LoadOptions()
    {
        mainPausedPanel.SetActive(false);
        optionsPanel.SetActive(true);
        p1Slider.Select();
    }
    public void Back()
    {
        mainPausedPanel.SetActive(true);
        mainPausedPanel.transform.GetChild(1).GetComponent<Button>().Select();
        optionsPanel.SetActive(false);

        float[] sensitivities = { p1Slider.value, p2Slider.value, p3Slider.value, p4Slider.value };
        matchSettingsObject.SetSensitivitySettings(sensitivities);
        MonitorPlayerManager.SINGLETON.UpdatePlayerSensitivities();
    }

    public void Slider1OnChange()
    {
        MonitorPlayerManager.SINGLETON.SetPlayerLookSensitivity(0, p1Slider.value);
    }
    public void Slider2OnChange()
    {
        MonitorPlayerManager.SINGLETON.SetPlayerLookSensitivity(1, p2Slider.value);
    }
    public void Slider3OnChange()
    {
        MonitorPlayerManager.SINGLETON.SetPlayerLookSensitivity(2, p3Slider.value);
    }
    public void Slider4OnChange()
    {
        MonitorPlayerManager.SINGLETON.SetPlayerLookSensitivity(3, p4Slider.value);
    }
    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters

    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        matchSettingsObject = GameObject.Find("MatchSettings").GetComponent<MatchSettings>();
        if (matchSettingsObject == null) PrintErrorDebugMsg("No match settings found!");

        p1Slider.value = MonitorPlayerManager.SINGLETON.GetPlayerLookSensitivity(0);
        p2Slider.value = MonitorPlayerManager.SINGLETON.GetPlayerLookSensitivity(1);
        p3Slider.value = MonitorPlayerManager.SINGLETON.GetPlayerLookSensitivity(2);
        p4Slider.value = MonitorPlayerManager.SINGLETON.GetPlayerLookSensitivity(3);
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}