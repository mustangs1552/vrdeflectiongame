﻿// (Unity3D) Script containg info about the game such as the game's current version and changelog. Includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class Versions : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    // Debugging
    public bool isDebug = false;
    private string debugScriptName = "Versions";
    #endregion

    #region Static
    public static Versions SINGLETON = null;
    #endregion

    #region Public

    #endregion

    #region Private
    private string releaseVer = "0";
    private string buildVer = "1";
    private string sprintNum = "8";
    private string currIteration = "50";

    private string ver = "";
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public

    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public string Version
    {
        get
        {
            return ver;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    public void OnGUI()
    {
        if (isDebug) GUI.Label(new Rect(0f, 0f, 100f, 50f), "Version: " + ver);
    }
    #endregion

    #region UnityMainFunctions
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        ver += releaseVer + "." + buildVer + "." + sprintNum + "." + currIteration;
        PrintDebugMsg("Game version: " + ver);

        if (Versions.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one Versions singleton detected.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {

    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}

/*Changelog
 * 
 * (4/6/17)
 * 0.0.1.1- Added VR player; Setup head and hands; Setup grabbing/dropping and throwing.
 * (4/7/17)
 * 0.0.1.2- Began working on adding the monitor players and setting them up.
 * 0.0.1.3- Monitor players are added and are able to move around.
 * (4/8/17)
 * 0.0.1.4- Finished health for both VR player and Monitor players; Finished Projectile but decided not to use it, as well as Weapon; Finished Laser; Finished Pistol and player is able to fire it.
 * (4/9/17)
 * 0.0.1.5- Finished deflection using Plasma Sword and VR player can activate/de-activate it.
 * (4/12/17)
 * 0.0.2.6- Started adding input for monitor player weapon selection.
 * 0.0.2.7- Player can now select weapons (currently only pistol available so all but up is no weapon).
 * (4/14/17)
 * 0.0.2.8- Started adding in the monitor player's invisibility by getting all the player's mesh renderers.
 * (4/15/17)
 * 0.0.2.9- Finished a simple version of player invisibility by simply disabling/enabling mesh renderers (Will want to add a better method later on primarliy to hav the monitor player still see some of thier gun); Added the timer.
 * (4/16/17)
 * 0.0.2.10- Started working on the assassin mode game controller, finished player spawning and gameflow with times. (Bug found where Time is only available for one player (at least for invisibility)).
 * (4/18/17)
 * 0.0.2.11- Fixed bug with visibility cooldown by switching from using Time.time to using coroutines.
 * (4/20/17)
 * 0.0.2.12- Rough prototype done; Able to play a round of assassin mode (needs work, however).
 * (4/21/17)
 * 0.0.3.13- Monitor players' jumping dust cloud code added; Weapon manager done.
 * 0.0.3.14- Weapon/ammo spawns now spawn at available points only; Added Ammo class; Weapons can now be re-supplied via pick-ups of the Ammo class; Monitor player interaction added.
 * (4/23/17)
 * 0.0.3.15- Added Assault Rifle and SMG. Fixed bug with weapon charging.
 * (4/25/17)
 * 0.0.4.16- Made simple dust cloud particle effects for walking and jumping.
 * (4/27/17)
 * 0.0.4.17- Finished simple monitor player dust clouds; Added models for guns.
 * (4/28/17)
 * 0.0.4.18- Made a missle with a particle effect, but doesn't damage yet; Started adding damage.
 * 0.0.4.19- Finished missle.
 * 0.0.4.20- Converted Pistol class to SemiAutoGun class.
 * 0.0.4.21- Finished missile launcher and fixed some other bugs.
 * (4/29/17)
 * 0.0.4.22- Added sounds for the monitor player; Made a simple map.
 * (4/30/17)
 * 0.0.4.23- Finished implementing holsters for VR player and plasma swords are teleporting back (Left hand doesn't seem to work right, however).
 * (5/2/17)
 * 0.0.5.24- Finished improving Assassin Game Mode flow.
 * (5/7/17)
 * 0.0.5.25- Fixed dust clouds spawning when walking in mid-air; Fixed movement.
 * 0.0.5.26- Added monitor player UI; Added VR player UI.
 * 0.0.5.27- Fixed VR player not able to press A to restart match.
 * 0.0.5.28- Tried to add limits on vertical look for monitor players.
 * (5/8/17)
 * 0.0.6.29- Created the main menu script (Unity crashed and lost main menu scene completly)
 * (5/9/17)
 * 0.0.6.30- Re-implemented lost menus; Added pause menu and functionality.
 * (5/11/17)
 * 0.0.7.31- Fixed problem with match settings not changing when starting a second match in the same session; Menus in main menu now support controller on monitor.
 * (5/13/17)
 * 0.0.7.32- Added limits to vertical looking.
 * (5/16/17)
 * 0.0.7.33- Fixed bug where analog sticks on controllers were returning max value only half the distance.
 * 0.0.7.34- Converted all public variables in classes to serialized private variables.
 * (5/22/17)
 * 0.1.8.38- Reverted back to version 0.0.7.34. Changelog before reverting:
 *                                                                          (5/20/17)
 *                                                                          0.0.7.35- Fixed bug with VR hands and holsters; Added global puase menu with options; Look sensitivities are saving between scenes also.
 *                                                                          (5/21/17)
 *                                                                          0.0.7.36- Added options menu for monitor players in main menu.
 *                                                                          0.1.8.37- Chnaged VR player's game lobby from drop downs to toggles (Broke out of nowhere) and disabled buttons on VR player's main menu.
 *           Added code back in from versions 0.0.7.35 - 0.0.7.36 but no testing yet.
 * (5/23/17)
 * 0.1.8.39- Finished putting the options menus back in and added functionalitiy to save settings locally.
 * (5/24/17)
 * 0.1.8.40- Added victory message for each player.
 * 0.1.8.41- Finished support for the player input state for assault game mode; Fixed bug with player sensitivities not saving in some screens.
 * (5/25/17)
 * 0.1.8.42- Player input and starting state working, but VR player input not working and monitor players' UI not working on start; EndGame code also added.
 * (5/26/17)
 * 0.1.8.43- Added code to try and fix VR player's hands dropping swords randomly (gameplay testing required); Finalized Player input and starting states for Assault mode.
 * (5/28/17)
 * 0.1.8.44- Added a respawn screen; Fixed UI for missile launcher counter; Added code to possibly reset weapons on death; Imporved falling and jumping; Players no longer jump when slecting UI.
 * 0.1.8.45- Items can now re-use spawnpoints immediately; Items will replace the oldest spawned weapon if no available spawnpoints and not re-using spawnpoints.
 * 0.1.8.46- Added a pickup message when looking at an item that can be picked up.
 * (5/29/17)
 * 0.1.8.47- Fixed monitor player respawning weapon bugs; Fixed weapon/ammo spawn error.
 * (5/30/17)
 * 0.1.8.48- Added VR player healthbar; Added potential fix for swords retracting randomly (needs testing).
 * (6/6/17)
 * 0.1.8.49- Added a hold down to activate on the VR player's triggers for held objects to potentially fix swords retracting enexpectedly bug.
 * (7/2/17)
 * 0.1.8.50- Removed ability to pick-up plasma swords and enable/disable them.
 */
