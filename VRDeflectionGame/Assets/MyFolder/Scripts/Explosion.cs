﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Explosion : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Explosion";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private float damage = -1;
    [SerializeField]
    private float radius = 5;
    [SerializeField]
    private float life = 1;
    #endregion

    #region Private
    private Team affiliation = Team.None;
    private float spawnTime = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public

    #endregion

    #region Private
    // Checks all the objects within radius that have health and calculates thier damage by how close they are to the center of the blast.
    private void Explode()
    {
        PrintDebugMsg("Exploding...");

        RaycastHit[] hits;
        hits = Physics.SphereCastAll(new Ray(transform.position, Vector3.forward), radius, radius);
        
        for (int i = 0; i < hits.Length; i++)
        {
            PrintDebugMsg("Checking #" + i + " " + hits[i].transform.name + " out of " + hits.Length + "...");

            if(hits[i].transform.GetComponent<Health>() != null && hits[i].transform.GetComponent<Health>().Affiliation != affiliation)
            {
                PrintDebugMsg("Hit a damagable object.");

                float damagePassed = Mathf.Abs((Vector3.Distance(transform.position, hits[i].transform.position) / radius) - 1);
                hits[i].transform.GetComponent<Health>().AdjustHealth(damagePassed * damage);
            }
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Team Affiliation
    {
        get
        {
            return affiliation;
        }
        set
        {
            affiliation = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        spawnTime = Time.time;

        Explode();
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        if (Time.time - spawnTime >= life) Destroy(gameObject);
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}