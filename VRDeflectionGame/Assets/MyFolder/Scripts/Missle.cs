﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class Missle : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Missle";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private GameObject explosionPrefab = null;
    [SerializeField]
    private float force = 1;
    #endregion

    #region Private
    private Team affiliation = Team.None;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public

    #endregion

    #region Private
    // Apply the initial force to start moving.
    private void ApplyInitialForce()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().AddForce(transform.forward * force * 100);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Team Affiliation
    {
        get
        {
            return affiliation;
        }
        set
        {
            affiliation = value;
        }
    }
    public GameObject ExplosionPrefab
    {
        get
        {
            return explosionPrefab;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlasmaSword>() != null)
        {
            PrintDebugMsg("Was hit by a plasma sword.");
            Destroy(gameObject);
        }
    }
    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        ApplyInitialForce();
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}