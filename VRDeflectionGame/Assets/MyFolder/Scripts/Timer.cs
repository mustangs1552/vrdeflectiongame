﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Timer";
    #endregion

    #region Static
    public static Timer SINGLETON = null;
    #endregion

    #region Public
    [SerializeField]
    private float timeMin = 2;
    [SerializeField]
    private GameObject[] objsToInform;
    #endregion

    #region Private
    private float currSec = 0;
    private float lastSec = 0;
    private bool isPaused = false;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Call to start the countdown. If timeMinSet is greater than 0 then set that to be the time limit.
    public void StartTimer(float timeMinSet = -1)
    {
        PrintDebugMsg("Starting timer at " + Time.time + ". Set to " + timeMin + " minutes.");

        if (timeMinSet > 0)
        {
            timeMin = timeMinSet;
            PrintDebugMsg("Timer changed to: " + timeMinSet);
        }
        currSec = 0;
        lastSec = Time.time;
    }

    // Return the current time as a string in the form of mm:ss either counting down or up.
    public string GetTimeString(bool countingDown)
    {
        if (countingDown)
        {
            float timeRemain = timeMin * 60 - currSec;
            return (int)(timeRemain / 60) + ":" + ((timeRemain % 60 < 10) ? "0" : "") + (timeRemain % 60);
        }
        else return (int)(currSec / 60) + ":" + ((currSec % 60 < 10) ? "0" : "") + (currSec % 60);
    }
    #endregion

    #region Private
    // Called when the time is up. This is where to put calls to other objects to inform them that the time is up.
    private void TimerEnd()
    {
        PrintDebugMsg("Time's up!");

        foreach (GameObject obj in objsToInform) obj.SendMessage("TimeUp");
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public bool IsPaused
    {
        set
        {
            IsPaused = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (Timer.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one Timer singletons detected!");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        if (!isPaused)
        {
            if (Time.time - lastSec >= 1)
            {
                currSec++;
                PrintDebugMsg(GetTimeString(true));
                lastSec = Time.time;
            }
            if (currSec >= timeMin * 60 - .1) TimerEnd();
        }
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}