﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class Pistol : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "Pistol";
    #endregion

    #region Static
    
    #endregion

    #region Public
    public GameObject projectile = null;
    public float cooldown = 2;
    [Range(0, 1)]
    public float chargeDepletionPerShot = .05f;
    [Range(0, 1)]
    public float passiveRechargeAmount = .05f;
    #endregion

    #region Private
    private float charge = 100;
    private float currCharge = 0;
    private float lastShot = 0;
    private Transform projSpawnPoint = null;
    private Transform shooter = null;
    private float lastCharge = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Fire the projectile and set its position and rotation to match the spawn point. Also set the affiliation and and parent it to the anchor.
    public void Fire()
    {
        if (Time.time - lastShot >= cooldown && currCharge >= chargeDepletionPerShot)
        {
            Transform laser = Instantiate(projectile, WeaponManager.MONITORPLAYER_PROJANCHOR).transform;
            laser.position = projSpawnPoint.position;
            laser.rotation = projSpawnPoint.rotation;
            laser.GetComponent<Laser>().Affiliation = Team.MonitorPlayer;
            laser.GetComponent<Laser>().Shooter = shooter;

            currCharge -= chargeDepletionPerShot * 100;
            lastShot = Time.time;
        }
    }

    // This object was just picked up by someone.
    public void OnPickUp(Transform pickedUpBy)
    {
        shooter = pickedUpBy;
    }

    // Re-supply this weapon's ammo type.
    public bool Resupply(float amount)
    {
        PrintDebugMsg("Charge: " + currCharge + "/" + charge);
        if (currCharge < charge)
        {
            currCharge += amount;
            if (currCharge > charge) currCharge = charge;
            return true;
        }

        return false;
    }
    #endregion

    #region Private
    // Sets up all the variables that need objects assigned.
    private void SetUpObjs()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "ProjSpawnPoint") projSpawnPoint = currChild;
        }
    }

    // Charges the weapon each second.
    private void PassiveCharge()
    {
        if (currCharge < charge && Time.time - lastCharge >= 1)
        {
            currCharge += passiveRechargeAmount * 100;
            if (currCharge > charge) currCharge = charge;

            lastCharge = Time.time;
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        SetUpObjs();

        lastShot = -cooldown;
        currCharge = charge;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        PassiveCharge();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}