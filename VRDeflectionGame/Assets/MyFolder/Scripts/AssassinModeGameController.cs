﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum AssassinModeState
{
    PlayerInput, // Pre-game. Wait til everyone presses a button or until timer runs out
    Starting,    // Give players a sec before match starts. Monitor players can start moving, but can't shoot.
    Playing,     // Game is playing
    EndGame,     // Game ended by either time running out or the VR player's death. Show the end screen.
    None
}

public class AssassinModeGameController : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "AssassinModeGameController";
    #endregion

    #region Static
    public static AssassinModeGameController SINGLETON = null;
    #endregion

    #region Public
    [SerializeField]
    private float playerInputTimerMin = 1;
    [SerializeField]
    private float gameStartTimerSec = 10;
    [SerializeField]
    private float gameplayTimerMin = 2;

    [SerializeField]
    private float monitorPlayerRespawnDelay = 3;

    [SerializeField]
    private TouchControllers vrPlayer = null;
    [SerializeField]
    private GameObject vrPausePanel = null;
    [SerializeField]
    private Text vrReadyText = null;
    [SerializeField]
    private Text vrTimerText = null;
    [SerializeField]
    private Text infoText = null;
    [SerializeField]
    private Text vrVictoryConditionText = null;
    [SerializeField]
    private float infoTextFadeDelay = 2;
    [SerializeField]
    private GameObject globalPauseCanvas = null;
    [SerializeField]
    private Selectable selectOnPauseLoad = null;

    [SerializeField]
    private bool vrPlayerReadyOnStart = false;
    #endregion

    #region Private
    private AssassinModeState state = AssassinModeState.None;

    private bool vrPlayerReady = false;
    private List<int> monitorPlayersReady = new List<int>();

    private string winner = "";

    private MatchSettings matchSettingsObject = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Called from the timer when the time runs out. Depending on the current state, some things are done and then moves on to the next state.
    public void TimeUp()
    {
        switch(state)
        {
            case AssassinModeState.PlayerInput:
                State = AssassinModeState.Starting;
                break;
            case AssassinModeState.Starting:
                State = AssassinModeState.Playing;
                break;
            case AssassinModeState.Playing:
                State = AssassinModeState.EndGame;
                break;
            case AssassinModeState.EndGame:
                MonitorPlayerManager.SINGLETON.Lose();
                vrVictoryConditionText.text = "You survived!";
                break;
        }
    }

    // Called from the players during the PlayerInput state when they are ready to start.
    public void PlayerReady(Team playerType, int monitorPlayer = -1)
    {
        if (playerType == Team.VRPlayer && !vrPlayerReady)
        {
            vrPlayerReady = true;
            PrintDebugMsg("VR player ready!");
        }
        else if (playerType == Team.MonitorPlayer)
        {
            bool newPlayer = true;
            foreach (int player in monitorPlayersReady)
            {
                if (monitorPlayer == player)
                {
                    newPlayer = false;
                    break;
                }
            }

            if (newPlayer)
            {
                monitorPlayersReady.Add(monitorPlayer);
                PrintDebugMsg("Monitor players ready: " + monitorPlayersReady.Count + "/" + MonitorPlayerManager.SINGLETON.NumbOfPlayers);
            }
        }

        if (vrPlayerReady && monitorPlayersReady.Count >= MonitorPlayerManager.SINGLETON.NumbOfPlayers)
        {
            if (state == AssassinModeState.PlayerInput) State = AssassinModeState.Starting;
            else if (state == AssassinModeState.EndGame) SceneManager.LoadScene("assassinGameMode");

            vrPlayerReady = vrPlayerReadyOnStart;
            monitorPlayersReady = new List<int>();
        }
    }

    // When a player dies they call this function and this determines what needs to be done.
    // VRPlayer: Game ends with monitor player's victory.
    // Monitor player: Monitor player respawns after monitorPlayerRespawnDelay time has passed.
    public void PlayerDied(Health playersHealth, Transform mostParent)
    {
        if(playersHealth.Affiliation == Team.VRPlayer)
        {
            PrintDebugMsg("Game over! Monitor players win!");
            State = AssassinModeState.EndGame;
            MonitorPlayerManager.SINGLETON.Win();
            vrVictoryConditionText.text = "You Died!";
        }
        else if(playersHealth.Affiliation == Team.MonitorPlayer)
        {
            PrintDebugMsg("Respawning a monitor player (" + mostParent.name + ")...");
            mostParent.Translate(Vector3.down * 100);
            StartCoroutine(RespawnPlayer(mostParent.gameObject));
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        MonitorPlayerManager.SINGLETON.PauseGame();
        vrPlayer.IsPaused = true;
        vrPausePanel.SetActive(true);

        globalPauseCanvas.SetActive(true);
        selectOnPauseLoad.Select();
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        MonitorPlayerManager.SINGLETON.ResumeGame();
        vrPlayer.IsPaused = false;
        vrPausePanel.SetActive(false);
        globalPauseCanvas.SetActive(false);
    }
    public void LoadMainMenu()
    {
        AssassinModeGameController.SINGLETON = null;
        SceneManager.LoadScene("mainMenu");
    }
    #endregion

    #region Private
    private IEnumerator RespawnPlayer(GameObject player)
    {
        yield return new WaitForSeconds(monitorPlayerRespawnDelay);
        
        MonitorPlayerManager.SINGLETON.SpawnPlayer(player);
    }

    private IEnumerator FadeInfoText()
    {
        yield return new WaitForSeconds(infoTextFadeDelay);
        infoText.gameObject.SetActive(false);
    }

    private void SetUpMatchSettings()
    {
        matchSettingsObject = GameObject.Find("MatchSettings").GetComponent<MatchSettings>();
        if (matchSettingsObject == null) PrintErrorDebugMsg("No match settings found!");

        MonitorPlayerManager.SINGLETON.NumbOfPlayers = matchSettingsObject.NumbOfPlayers;
        gameplayTimerMin = matchSettingsObject.TimeLimit;
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public AssassinModeState State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
            PrintDebugMsg("State is now: " + state);

            switch(state)
            {
                case AssassinModeState.PlayerInput:
                    //Timer.SINGLETON.StartTimer(playerInputTimerMin);
                    GetComponent<MonitorPlayerManager>().SpawnAllPlayers();
                    GetComponent<MonitorPlayerManager>().ShowPlayers();
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);
                    
                    vrReadyText.gameObject.SetActive(true);
                    vrTimerText.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(false);
                    vrVictoryConditionText.gameObject.SetActive(false);
                    break;
                case AssassinModeState.Starting:
                    Timer.SINGLETON.StartTimer(gameStartTimerSec / 60);
                    GetComponent<MonitorPlayerManager>().HidePlayers();
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    vrReadyText.gameObject.SetActive(false);
                    vrTimerText.gameObject.SetActive(true);
                    infoText.text = "Match starting...";
                    infoText.gameObject.SetActive(true);
                    vrVictoryConditionText.gameObject.SetActive(false);
                    break;
                case AssassinModeState.Playing:
                    Timer.SINGLETON.StartTimer(gameplayTimerMin);
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    infoText.text = "Survive!";
                    vrVictoryConditionText.gameObject.SetActive(false);
                    StartCoroutine(FadeInfoText());
                    break;
                case AssassinModeState.EndGame:
                    //Timer.SINGLETON.StartTimer(gameStartTimerSec / 60);
                    GetComponent<MonitorPlayerManager>().ShowPlayers();
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    vrReadyText.gameObject.SetActive(true);
                    vrTimerText.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(false);
                    vrVictoryConditionText.gameObject.SetActive(true);
                    break;
            }
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (AssassinModeGameController.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one AssassinModeGameController singletons detected!");

        SetUpMatchSettings();
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        State = AssassinModeState.PlayerInput;
        vrPlayerReady = vrPlayerReadyOnStart;
        vrPausePanel.SetActive(false);

        ResumeGame();
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        if(state == AssassinModeState.PlayerInput) vrReadyText.text = (vrPlayerReady) ? "Ready!" : "Press \'A\' to Ready Up!";
        else if (state == AssassinModeState.EndGame) vrReadyText.text = (vrPlayerReady) ? "Ready!" : "Press \'A\' to Continue!";
        vrTimerText.text = Timer.SINGLETON.GetTimeString(true);
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}