﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ItemType
{
    Weapon,
    Ammo,
    None
}

public class WeaponManager : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "WeaponManager";
    #endregion

    #region Static
    public static WeaponManager SINGLETON = null;
    public static Transform MONITORPLAYER_PROJANCHOR = null;
    #endregion

    #region Public
    [SerializeField]
    private float weaponSpawnCooldown = 10;
    [SerializeField]
    private float ammoSpawnCooldown = 10;

    [SerializeField]
    private List<Spawnable> spawnableWeapons = new List<Spawnable>();
    [SerializeField]
    private List<Spawnable> spawnableAmmo = new List<Spawnable>();

    [SerializeField]
    private bool spawnsResusableImmediately = false;
    #endregion

    #region Private
    private List<Transform> weaponSpawns = new List<Transform>();
    private List<Transform> ammoSpawns = new List<Transform>();
    private Transform[] spawnedWeapons;
    private List<Transform> spawnedWeaponsAge = new List<Transform>();
    private Transform[] spawnedAmmo;
    private List<Transform> spawnedAmmoAge = new List<Transform>();

    private float lastWeaponSpawn = 0;
    private float lastAmmoSpawn = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Removes the given weapon/ammo from its linked point so that point can spawn another.
    public void RemoveWeaponFromPoint(Transform weapon)
    {
        for (int i = 0; i < spawnedWeapons.Length; i++)
        {
            if (spawnedWeapons[i] == weapon) spawnedWeapons[i] = null;
        }
        for (int i = 0; i < spawnedWeaponsAge.Count; i++)
        {
            if (spawnedWeaponsAge[i] == weapon) spawnedWeaponsAge.RemoveAt(i);
        }
    }
    public void RemoveAmmoFromPoint(Transform ammo)
    {
        for(int i = 0; i < spawnedAmmo.Length; i++)
        {
            if (spawnedAmmo[i] == ammo) spawnedAmmo[i] = null;
        }
        for (int i = 0; i < spawnedAmmoAge.Count; i++)
        {
            if (spawnedAmmoAge[i] == ammo) spawnedAmmoAge.RemoveAt(i);
        }
    }
    #endregion

    #region Private
    // Sets up the list of weapon and ammo spawn points.
    private void FindSpawns()
    {
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("WeaponSpawn");
        foreach (GameObject spawn in spawns) weaponSpawns.Add(spawn.transform);

        PrintDebugMsg("Found " + weaponSpawns.Count + " weapon spawn point(s).");

        spawns = GameObject.FindGameObjectsWithTag("AmmoSpawn");
        foreach (GameObject spawn in spawns) ammoSpawns.Add(spawn.transform);

        PrintDebugMsg("Found " + ammoSpawns.Count + " ammo spawn point(s).");
    }

    // Calculates chances for which weapon/Ammo will spawn. Then picks a random weapon/Ammo spawnpoint to spawn it at that doesn't already have a weapon/ammo still there.
    private void SpawnWeapon()
    {
        List<Transform> availableSpawnpoints = new List<Transform>();
        int[] availableSpawnsI = new int[weaponSpawns.Count];
        int currI = 0;
        for(int i = 0; i < weaponSpawns.Count; i++)
        {
            availableSpawnsI[i] = -1;
            if (spawnedWeapons[i] == null)
            {
                availableSpawnpoints.Add(weaponSpawns[i]);
                availableSpawnsI[i] = currI;
                currI++;
            }
        }
        if(availableSpawnpoints.Count == 0 && !spawnsResusableImmediately)
        {
            GameObject removedItem = spawnedWeaponsAge[0].gameObject;
            RemoveWeaponFromPoint(spawnedWeaponsAge[0]);
            Destroy(removedItem);

            StartCoroutine(ReRunSpawnWeapon());
            return;
        }

        if (availableSpawnpoints.Count > 0)
        {
            int totalChances = 0;
            foreach (Spawnable weapon in spawnableWeapons) totalChances += weapon.chance;

            int randNum = Random.Range(0, totalChances + 1);
            PrintDebugMsg("Random number: " + randNum + "/" + totalChances);
            int prevChances = 0;
            foreach (Spawnable weapon in spawnableWeapons)
            {
                if (randNum >= prevChances && randNum <= prevChances + weapon.chance)
                {
                    PrintDebugMsg("Spawning " + weapon.item.name + " (Range: " + prevChances + "-" + (prevChances + weapon.chance) + ")...");

                    randNum = Random.Range(0, availableSpawnpoints.Count);
                    for(int i = 0; i < availableSpawnsI.Length; i++)
                    {
                        if (randNum == availableSpawnsI[i])
                        {
                            randNum = i;
                            break;
                        }
                    }
                    spawnedWeapons[randNum] = Instantiate(weapon.item, weaponSpawns[randNum].position, weaponSpawns[randNum].rotation).transform;
                    if (spawnsResusableImmediately) RemoveWeaponFromPoint(spawnedWeapons[randNum]);
                    else spawnedWeaponsAge.Add(spawnedWeapons[randNum]);

                    break;
                }

                prevChances += weapon.chance;
            }
        }
    }
    private void SpawnAmmo()
    {
        List<Transform> availableSpawnpoints = new List<Transform>();
        int[] availableSpawnsI = new int[ammoSpawns.Count];
        int currI = 0;
        for (int i = 0; i < ammoSpawns.Count; i++)
        {
            availableSpawnsI[i] = -1;
            if (spawnedAmmo[i] == null)
            {
                availableSpawnpoints.Add(ammoSpawns[i]);
                availableSpawnsI[i] = currI;
                currI++;
            }
        }
        if (availableSpawnpoints.Count == 0 && !spawnsResusableImmediately)
        {
            RemoveAmmoFromPoint(spawnedAmmoAge[0]);
            GameObject removedItem = spawnedAmmoAge[0].gameObject;
            Destroy(removedItem);

            StartCoroutine(ReRunSpawnAmmo());
            return;
        }

        if (availableSpawnpoints.Count > 0)
        {
            int totalChances = 0;
            foreach (Spawnable ammo in spawnableAmmo) totalChances += ammo.chance;

            int randNum = Random.Range(0, totalChances + 1);
            PrintDebugMsg("Random number: " + randNum + "/" + totalChances);
            int prevChances = 0;
            foreach (Spawnable ammo in spawnableAmmo)
            {
                if (randNum >= prevChances && randNum <= prevChances + ammo.chance)
                {
                    PrintDebugMsg("Spawning " + ammo.item.name + " (Range: " + prevChances + "-" + (prevChances + ammo.chance) + ")...");

                    randNum = Random.Range(0, availableSpawnpoints.Count);
                    for (int i = 0; i < availableSpawnsI.Length; i++)
                    {
                        if (randNum == availableSpawnsI[i])
                        {
                            randNum = i;
                            break;
                        }
                    }
                    spawnedAmmo[randNum] = Instantiate(ammo.item, ammoSpawns[randNum].position, ammoSpawns[randNum].rotation).transform;
                    if (spawnsResusableImmediately) RemoveAmmoFromPoint(spawnedAmmo[randNum]);
                    else spawnedAmmoAge.Add(spawnedAmmo[randNum]);

                    break;
                }

                prevChances += ammo.chance;
            }
        }
    }

    private IEnumerator ReRunSpawnWeapon()
    {
        yield return new WaitForSeconds(.1f);
        SpawnWeapon();
    }
    private IEnumerator ReRunSpawnAmmo()
    {
        yield return new WaitForSeconds(.1f);
        SpawnAmmo();
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters

    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (WeaponManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one WeaponManager singletons detected!");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        if (MONITORPLAYER_PROJANCHOR == null) MONITORPLAYER_PROJANCHOR = new GameObject("MONITORPLAYER_PROJANCHOR").transform;
        else PrintWarningDebugMsg("More than one projectile anchors in scene!");

        FindSpawns();
        spawnedWeapons = new Transform[weaponSpawns.Count];
        spawnedAmmo = new Transform[ammoSpawns.Count];
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        if(Time.time - lastWeaponSpawn >= weaponSpawnCooldown && spawnableWeapons.Count > 0)
        {
            SpawnWeapon();
            lastWeaponSpawn = Time.time;
        }
        if (Time.time - lastAmmoSpawn >= ammoSpawnCooldown && spawnableAmmo.Count > 0)
        {
            SpawnAmmo();
            lastAmmoSpawn = Time.time;
        }
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}

[System.Serializable]
public struct Spawnable
{
    public GameObject item;
    public int chance;
}