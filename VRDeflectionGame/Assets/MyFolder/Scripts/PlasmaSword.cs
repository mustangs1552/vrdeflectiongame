﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class PlasmaSword : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "PlasmaSword";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private float swordYInActive = .05f;
    [SerializeField]
    private float swordYActive = .5f;
    [SerializeField]
    private float swordStateChangeRate = .1f;

    [SerializeField]
    private float colliderActiveHY = 1;
    [SerializeField]
    private float colliderInActiveHY = .2f;
    [SerializeField]
    private float colliderActivePosY = 0;
    [SerializeField]
    private float colliderInActivePosY = -.4f;

    [SerializeField]
    private VRPlayerHolster holsterOne = null;
    [SerializeField]
    private VRPlayerHolster holsterTwo = null;
    [SerializeField]
    private float maxDistFromPlayer = 5;
    [SerializeField]
    private float maxTimeNotGrabbed = 5;
    #endregion

    #region Private
    private Transform holder = null;
    private bool active = true;
    private Transform sword = null;
    private float toggleCooldown = .25f;
    private float lastToggle = 0;
    private CapsuleCollider capCollider = null;

    private float lastGrabbed = 0;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Toggle the swords active state. forceActive is if you want to force the state to be a certain state and not just toggle (0 = force false, 1 = force true, any other = toggle).
    public void ActivateDeactivate(int forceActive = 2)
    {
        if (Time.time - lastToggle >= toggleCooldown)
        {
            if (active || forceActive == 0)
            {
                active = false;
                lastToggle = Time.time;
            }
            else if (!active || forceActive == 1)
            {
                active = true;
                lastToggle = Time.time;
            }
        }
    }
    
    public void OnDropped()
    {
        lastGrabbed = Time.time;
        holder = null;
        //ActivateDeactivate(0);
    }
    #endregion

    #region Private
    // If the sword is not fully in its current state then move to that state.
    private void ChangeState()
    {
        PrintDebugMsg("Checking state...");

        if (active && sword.localScale.y < swordYActive)
        {
            PrintDebugMsg("Activating...");

            sword.localScale = new Vector3(sword.localScale.x, sword.localScale.y + swordStateChangeRate * Time.deltaTime, sword.localScale.z);
            sword.Translate(Vector3.up * (swordStateChangeRate * Time.deltaTime));

            capCollider.height = colliderActiveHY;
            capCollider.center = new Vector3(0, colliderActivePosY, 0);
        }
        else if (!active && sword.localScale.y > swordYInActive)
        {
            PrintDebugMsg("De-activating...");

            sword.localScale = new Vector3(sword.localScale.x, sword.localScale.y - swordStateChangeRate * Time.deltaTime, sword.localScale.z);
            sword.Translate(Vector3.down * (swordStateChangeRate * Time.deltaTime));

            capCollider.height = colliderInActiveHY;
            capCollider.center = new Vector3(0, colliderInActivePosY, 0);
        }
    }

    private void CheckAway()
    {
        if (holsterOne.HeldObj != transform && holsterTwo.HeldObj != transform && holder == null)
        {
            if (Time.time - lastGrabbed >= maxTimeNotGrabbed)
            {
                if (holsterOne.HeldObj == null) holsterOne.InsertObj(transform);
                else if (holsterTwo.HeldObj == null) holsterTwo.InsertObj(transform);
            }
            if(Vector3.Distance(transform.position, Vector3.Lerp(holsterOne.transform.position, holsterTwo.transform.position, .5f)) >= maxDistFromPlayer)
            {
                if (holsterOne.HeldObj == null) holsterOne.InsertObj(transform);
                else if (holsterTwo.HeldObj == null) holsterTwo.InsertObj(transform);
            }
        }
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Transform Holder
    {
        get
        {
            return holder;
        }
        set
        {
            holder = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    private void OnTriggerEnter(Collider other)
    {
        PrintDebugMsg("Triggered " + other.gameObject.name + "!");

        if(other.gameObject.GetComponent<Laser>() != null && other.gameObject.GetComponent<Laser>().Affiliation != Team.VRPlayer)
        {
            other.gameObject.GetComponent<Laser>().Redirect(holder);
        }
    }
    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        lastToggle = toggleCooldown;

        if (swordStateChangeRate > 2) swordStateChangeRate = 2;
        else if (swordStateChangeRate < 0) swordStateChangeRate = 0;

        capCollider = GetComponent<CapsuleCollider>();

        for(int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "Sword") sword = currChild;
        }
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        ChangeState();
        //CheckAway();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}