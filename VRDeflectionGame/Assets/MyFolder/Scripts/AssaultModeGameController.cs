﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum AssaultState
{
    PlayerInput,
    Starting,
    Playing,
    PostRound,
    EndGame,
    None
}

public class AssaultModeGameController : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "AssaultModeGameController";
    #endregion

    #region Static
    public static AssaultModeGameController SINGLETON = null;
    #endregion

    #region Public
    [SerializeField]
    private float gameStartTimerSec = 10;

    [SerializeField]
    private TouchControllers vrPlayer = null;
    [SerializeField]
    private GameObject vrPausePanel = null;
    [SerializeField]
    private Text vrReadyText = null;
    [SerializeField]
    private Text vrTimerText = null;
    [SerializeField]
    private Text infoText = null;
    [SerializeField]
    private float infoTextFadeDelay = 3;
    [SerializeField]
    private Text vrVictoryConditionText = null;
    [SerializeField]
    private GameObject globalPauseCanvas = null;
    [SerializeField]
    private Selectable selectOnPauseLoad = null;

    [SerializeField]
    private bool vrPlayerReadyOnStart = false;
    #endregion

    #region Private
    private AssaultState state = AssaultState.None;
    private List<int> monitorPlayersReady = new List<int>();
    private bool vrPlayerReady = false;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Called from the players during the PlayerInput state when they are ready to start.
    public void PlayerReady(Team playerType, int monitorPlayer = -1)
    {
        if (playerType == Team.VRPlayer && !vrPlayerReady)
        {
            vrPlayerReady = true;
            PrintDebugMsg("VR player ready!");
        }
        else if (playerType == Team.MonitorPlayer)
        {
            bool newPlayer = true;
            foreach (int player in monitorPlayersReady)
            {
                if (monitorPlayer == player)
                {
                    newPlayer = false;
                    break;
                }
            }

            if (newPlayer)
            {
                monitorPlayersReady.Add(monitorPlayer);
                PrintDebugMsg("Monitor players ready: " + monitorPlayersReady.Count + "/" + MonitorPlayerManager.SINGLETON.NumbOfPlayers);
            }
        }

        if (vrPlayerReady && monitorPlayersReady.Count >= MonitorPlayerManager.SINGLETON.NumbOfPlayers)
        {
            if (state == AssaultState.PlayerInput) State = AssaultState.Starting;
            //else if (state == AssaultState.EndGame) SceneManager.LoadScene("assassinGameMode");

            vrPlayerReady = vrPlayerReadyOnStart;
            monitorPlayersReady = new List<int>();
        }
    }

    public void TimeUp()
    {
        switch(state)
        {
            case AssaultState.Starting:
                State = AssaultState.Playing;
                break;
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        MonitorPlayerManager.SINGLETON.PauseGame();
        vrPlayer.IsPaused = true;
        vrPausePanel.SetActive(true);

        globalPauseCanvas.SetActive(true);
        selectOnPauseLoad.Select();
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        MonitorPlayerManager.SINGLETON.ResumeGame();
        vrPlayer.IsPaused = false;
        vrPausePanel.SetActive(false);
        globalPauseCanvas.SetActive(false);
    }
    public void LoadMainMenu()
    {
        AssassinModeGameController.SINGLETON = null;
        SceneManager.LoadScene("mainMenu");
    }
    #endregion

    #region Private
    private IEnumerator FadeInfoText()
    {
        yield return new WaitForSeconds(infoTextFadeDelay);
        infoText.gameObject.SetActive(false);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public AssaultState State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
            switch(state)
            {
                case AssaultState.PlayerInput:
                    PrintDebugMsg("State = " + state);
                    GetComponent<MonitorPlayerManager>().SpawnAllPlayers();
                    GetComponent<MonitorPlayerManager>().ShowPlayers();
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    vrReadyText.gameObject.SetActive(true);
                    vrTimerText.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(false);
                    vrVictoryConditionText.gameObject.SetActive(false);
                    break;
                case AssaultState.Starting:
                    PrintDebugMsg("State = " + state);
                    Timer.SINGLETON.StartTimer(gameStartTimerSec / 60);
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    vrReadyText.gameObject.SetActive(false);
                    vrTimerText.gameObject.SetActive(true);
                    infoText.text = "Round starting...";
                    infoText.gameObject.SetActive(true);
                    vrVictoryConditionText.gameObject.SetActive(false);
                    break;
                case AssaultState.Playing:
                    PrintDebugMsg("State = " + state);
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    infoText.text = "Survive!";
                    StartCoroutine(FadeInfoText());
                    vrTimerText.gameObject.SetActive(false);
                    vrVictoryConditionText.gameObject.SetActive(false);
                    break;
                case AssaultState.PostRound:
                    PrintDebugMsg("State = " + state);
                    // Start enemy selection
                    break;
                case AssaultState.EndGame:
                    PrintDebugMsg("State = " + state);
                    GetComponent<MonitorPlayerManager>().TellPlayersStateChanged(state);

                    vrReadyText.gameObject.SetActive(true);
                    vrTimerText.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(false);
                    vrVictoryConditionText.gameObject.SetActive(true);
                    break;
            }
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (AssaultModeGameController.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("More than one AssaultModeGameController detected!");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        State = AssaultState.PlayerInput;
        vrPlayerReady = vrPlayerReadyOnStart;
        vrPausePanel.gameObject.SetActive(false);
        
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        vrTimerText.text = Timer.SINGLETON.GetTimeString(true);
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}