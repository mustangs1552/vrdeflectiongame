﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MonitorPlayer : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "MonitorPlayer";
    #endregion

    #region Static

    #endregion

    #region Public
    [Header("Movement")]
    [SerializeField]
    private float speed = 5;
    [SerializeField]
    private float lookSpeed = 10;
    [SerializeField]
    private float minLookX = -90;
    [SerializeField]
    private float maxLookX = 90;
    [SerializeField]
    private float jumpForce = 1;
    [SerializeField]
    private float aButtonDelay = .5f;
    [SerializeField]
    private float interactRange = 2;

    [Header("Invisibility")]
    [SerializeField]
    private float visibleLength = 2;

    [Header("Dust Clouds")]
    [SerializeField]
    private GameObject jumpingDustCloud = null;
    [SerializeField]
    private GameObject walkingDustCloud = null;
    [SerializeField]
    private float walkingDustCloudFrequency = 1;

    [Header("UI")]
    [SerializeField]
    private Text readyUpText = null;
    [SerializeField]
    private GameObject hudPanel = null;
    [SerializeField]
    private Text timerText = null;
    [SerializeField]
    private Text infoText = null;
    [SerializeField]
    private float infoTextFadeDelay = 2;
    [SerializeField]
    private GameObject respawnPanel = null;
    [SerializeField]
    private Text pickupUI = null;
    [SerializeField]
    private Text victoryConditionText = null;
    #endregion

    #region Private
    private Camera cam = null;
    private int playerNum = 0;
    private bool grounded = true;
    private MonitorPlayerWeapons weapons = null;
    private List<MeshRenderer> meshRenderors = new List<MeshRenderer>();
    private float lastStep = 0;
    private bool triggerDown = false;
    private bool isPaused = false;
    private float currRotX = 0;
    private float lastA = 0;
    private float fallingSpeedMultiplier = 1;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Set-up function. Called from other objects parented to this player to add thier mesh renderers to the meshRenderer list to be controlled by visibilty.
    public void SetRenderers(List<MeshRenderer> renderers)
    {
        foreach (MeshRenderer renderer in renderers) meshRenderors.Add(renderer);
        BecomeInvisible();
    }

    // Change visibility by disabling/enabling mesh renderers. BecomeInvisbleDelay() has a delay that represents the visibility cooldown while BecomeInvisible() doesn't.
    public void BecomeVisible(bool autoHide = true)
    {
        foreach (MeshRenderer renderer in meshRenderors) renderer.enabled = true;

        if (autoHide)
        {
            StopAllCoroutines();
            StartCoroutine(BecomeInvisibleDelay());
        }
    }
    public IEnumerator BecomeInvisibleDelay()
    {
        yield return new WaitForSeconds(visibleLength);
        foreach (MeshRenderer renderer in meshRenderors) renderer.enabled = false;
    }
    public void BecomeInvisible()
    {
        foreach (MeshRenderer renderer in meshRenderors) renderer.enabled = false;
    }

    // Called when the state was just changed. Chnages the UI shown to the proper UI for that state.
    public void StateChange(AssassinModeState state)
    {
        switch(state)
        {
            case AssassinModeState.PlayerInput:
                readyUpText.text = "Press \'A\' to Ready Up!";
                readyUpText.gameObject.SetActive(true);
                hudPanel.SetActive(false);
                victoryConditionText.gameObject.SetActive(false);
                break;
            case AssassinModeState.Starting:
                readyUpText.gameObject.SetActive(false);
                hudPanel.SetActive(true);
                infoText.text = "Match Starting...";
                infoText.gameObject.SetActive(true);
                victoryConditionText.gameObject.SetActive(false);
                break;
            case AssassinModeState.Playing:
                infoText.text = "Kill the VR Player!";
                infoText.gameObject.SetActive(true);
                victoryConditionText.gameObject.SetActive(false);
                StartCoroutine(FadeInfoText());
                break;
            case AssassinModeState.EndGame:
                readyUpText.text = "Press \'A\' to Continue!";
                readyUpText.gameObject.SetActive(true);
                hudPanel.SetActive(false);
                victoryConditionText.gameObject.SetActive(true);
                break;
        }
    }
    public void StateChange(AssaultState state)
    {
        switch (state)
        {
            case AssaultState.PlayerInput:
                readyUpText.text = "Press \'A\' to Ready Up!";
                readyUpText.gameObject.SetActive(true);
                hudPanel.SetActive(false);
                infoText.gameObject.SetActive(false);
                victoryConditionText.gameObject.SetActive(false);
                break;
            case AssaultState.Starting:
                readyUpText.gameObject.SetActive(false);
                hudPanel.SetActive(true);
                infoText.text = "Round Starting...";
                infoText.gameObject.SetActive(true);
                victoryConditionText.gameObject.SetActive(false);
                break;
            case AssaultState.Playing:
                infoText.text = "Kill the VR Player!";
                infoText.gameObject.SetActive(true);
                timerText.gameObject.SetActive(false);
                victoryConditionText.gameObject.SetActive(false);
                StartCoroutine(FadeInfoText());
                break;
            case AssaultState.EndGame:
                break;
        }
    }

    public void OnSpawn()
    {
        respawnPanel.SetActive(false);
    }

    public void Pause()
    {
        isPaused = true;
    }
    public void Resume()
    {
        lastA = Time.time;
        isPaused = false;
    }

    public void Win()
    {
        victoryConditionText.text = "The VR player was killed!";
    }
    public void Lose()
    {
        victoryConditionText.text = "The VR player survived!";
    }
    #endregion

    #region Private
    // Looks through the children and sets the ones to thier proper variables.
    private void SetUpObjects()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "Camera") cam = transform.GetChild(i).GetComponent<Camera>();

            if (currChild.GetComponent<MeshRenderer>() != null) meshRenderors.Add(currChild.GetComponent<MeshRenderer>());
        }
    }

    // Handles walking around the world using the left analog stick.
    private void Movement()
    {
        //PrintDebugMsg("Player " + playerNum + " is moving!");

        float x = Input.GetAxis("Horizontal_P" + playerNum.ToString());
        float z = -Input.GetAxis("Vertical_P" + playerNum.ToString());
        GetComponent<Rigidbody>().AddForce(transform.right * x * speed);
        GetComponent<Rigidbody>().AddForce(transform.forward * z * speed);

        if (Time.time - lastStep >= walkingDustCloudFrequency && grounded)
        {
            Instantiate(walkingDustCloud, new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2), transform.position.z), Quaternion.identity);
            lastStep = Time.time;
        }
    }
    // Handles looking around using the right analog stick.
    private void Look()
    {
        float x = Input.GetAxis("LookX_P" + playerNum.ToString());
        float y = Input.GetAxis("LookY_P" + playerNum.ToString());
        cam.transform.Rotate(new Vector3(Exponent(y, 3) * lookSpeed, 0, 0));
        transform.Rotate(new Vector3(0, Exponent(x, 3) * lookSpeed, 0));

        currRotX += Exponent(y, 3) * lookSpeed;
        if(currRotX > maxLookX)
        {
            //PrintDebugMsg("Reached max limit. " + currRotX + " > " + maxLookX);
            cam.transform.Rotate(new Vector3(-(Exponent(y, 3) * lookSpeed), 0, 0));
            currRotX += -(Exponent(y, 3) * lookSpeed);
        }
        else if (currRotX < minLookX)
        {
            //PrintDebugMsg("Reached min limit. " + currRotX + " < " + minLookX);
            cam.transform.Rotate(new Vector3(-(Exponent(y, 3) * lookSpeed), 0, 0));
            currRotX += -(Exponent(y, 3) * lookSpeed);
        }
    }
    // Applies a force upward when the A buttone is used
    private void Jump()
    {
        PrintDebugMsg("Jumping...");
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
        grounded = false;
    }
    // Find the object that the player is looking at and call its interact function.
    private void Interact()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, cam.transform.forward, out hit))
        {
            PrintDebugMsg("Hit " + hit.transform.name);
            if (Vector3.Distance(transform.position, hit.transform.position) <= interactRange)
            {
                bool success = false;

                if (hit.transform.GetComponent<Ammo>() != null)
                {
                    PrintDebugMsg("Picking up ammo...");
                    success = GetComponent<MonitorPlayerWeapons>().ResupplyWeapon(hit.transform.GetComponent<Ammo>());
                    if (success)
                    {
                        WeaponManager.SINGLETON.RemoveAmmoFromPoint(hit.transform);
                        Destroy(hit.transform.gameObject);
                    }
                }
                else if(hit.transform.GetComponent<FullAutoGun>() != null)
                {
                    PrintDebugMsg("Picking up a full-auto gun...");
                    success = GetComponent<MonitorPlayerWeapons>().PickupWeapon(hit.transform);
                    if(success)
                    {
                        WeaponManager.SINGLETON.RemoveWeaponFromPoint(hit.transform);
                        Destroy(hit.transform.gameObject);
                    }
                }
                else if (hit.transform.GetComponent<OneShotReloadGun>() != null)
                {
                    PrintDebugMsg("Picking up a one-shot reload gun...");
                    success = GetComponent<MonitorPlayerWeapons>().PickupWeapon(hit.transform);
                    if (success)
                    {
                        WeaponManager.SINGLETON.RemoveWeaponFromPoint(hit.transform);
                        Destroy(hit.transform.gameObject);
                    }
                }
            }
        }
    }

     // Different firing actions.
    // Trigger pressed
    private void FireDown()
    {
        PrintDebugMsg("Started firing.");
        weapons.FireWeapon(WeaponAction.Start);
        triggerDown = true;
    }
    // Trigger still pressed
    private void Firing()
    {
        PrintDebugMsg("Firing...");
        weapons.FireWeapon(WeaponAction.Continuing);
    }
    // Trigger no longer pressed
    private void FireUp()
    {
        PrintDebugMsg("Stoped firing.");
        weapons.FireWeapon(WeaponAction.Stop);
        triggerDown = false;
    }

    // Checks the input from the input device.
    private void CheckInput()
    {
        if (!isPaused)
        {
            AssassinModeState assassinModeState = AssassinModeState.None;
            if (AssassinModeGameController.SINGLETON != null) assassinModeState = AssassinModeGameController.SINGLETON.State;
            AssaultState assaultState = AssaultState.None;
            if (AssaultModeGameController.SINGLETON != null) assaultState = AssaultModeGameController.SINGLETON.State;
            if ((assassinModeState == AssassinModeState.PlayerInput || assassinModeState == AssassinModeState.EndGame) || (assaultState == AssaultState.PlayerInput || assaultState == AssaultState.EndGame))
            {
                if (Input.GetAxis("Jump_P" + playerNum.ToString()) != 0 && Time.time - lastA >= aButtonDelay)
                {
                    PrintDebugMsg("Readying up...");
                    if (AssassinModeGameController.SINGLETON != null) AssassinModeGameController.SINGLETON.PlayerReady(Team.MonitorPlayer, playerNum);
                    if (AssaultModeGameController.SINGLETON != null) AssaultModeGameController.SINGLETON.PlayerReady(Team.MonitorPlayer, playerNum);
                    readyUpText.text = "Ready!";
                    lastA = Time.time;
                }
            }

            if (assassinModeState != AssassinModeState.PlayerInput && assassinModeState != AssassinModeState.EndGame && assaultState != AssaultState.PlayerInput && assaultState != AssaultState.EndGame)
            {
                readyUpText.gameObject.SetActive(false);
                hudPanel.SetActive(true);

                if (Mathf.Abs(Input.GetAxis("Horizontal_P" + playerNum.ToString())) >= .2 || Mathf.Abs(Input.GetAxis("Vertical_P" + playerNum.ToString())) >= .2) Movement();
                if (Mathf.Abs(Input.GetAxis("LookX_P" + playerNum.ToString())) >= 0 || Mathf.Abs(Input.GetAxis("LookY_P" + playerNum.ToString())) >= 0) Look();
                if (Input.GetAxis("Jump_P" + playerNum.ToString()) != 0 && grounded && Time.time - lastA >= aButtonDelay)
                {
                    Jump();
                    lastA = Time.time;
                }

                if (Input.GetAxis("Fire_P" + playerNum.ToString()) >= .2 && assassinModeState != AssassinModeState.Starting && assaultState != AssaultState.Starting)
                {
                    PrintDebugMsg("Player " + playerNum + " pulled the right trigger.");
                    if (!triggerDown) FireDown();
                    else Firing();
                }
                else if (triggerDown) FireUp();

                if (Input.GetAxis("DPadX_P" + playerNum.ToString()) < 0) weapons.SelectWeapon(Vector2.left);
                if (Input.GetAxis("DPadX_P" + playerNum.ToString()) > 0) weapons.SelectWeapon(Vector2.right);
                if (Input.GetAxis("DPadY_P" + playerNum.ToString()) > 0) weapons.SelectWeapon(Vector2.up);
                if (Input.GetAxis("DPadY_P" + playerNum.ToString()) < 0) weapons.SelectWeapon(Vector2.down);

                if (Input.GetAxis("Interact_P" + playerNum.ToString()) != 0) Interact();
            }

            if (Input.GetKeyUp("joystick " + playerNum.ToString() + " button 7"))
            {
                if(AssassinModeGameController.SINGLETON != null) AssassinModeGameController.SINGLETON.PauseGame();
                else if (AssaultModeGameController.SINGLETON != null) AssaultModeGameController.SINGLETON.PauseGame();
            }
        }
    }

    // Uses a raycast to check to see if making contact with the ground and sets the grounded boolean.
    private void CheckGrounded()
    {
        if(!grounded)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.transform.tag == "Ground" && Vector3.Distance(transform.position, hit.point) <= transform.localScale.y)
                {
                    grounded = true;
                    fallingSpeedMultiplier = 1;
                    if (Input.GetAxis("Jump_P" + playerNum.ToString()) == 0) Instantiate(jumpingDustCloud, hit.point, Quaternion.identity);
                }
            }
        }
        else
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.transform.tag == "Ground" && Vector3.Distance(transform.position, hit.point) <= transform.localScale.y) grounded = true;//PrintDebugMsg("Found ground!");
                else grounded = false;
            }
            else grounded = false;
        }
    }

    private void CheckPickupUI()
    {
        pickupUI.gameObject.SetActive(false);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, cam.transform.forward, out hit))
        {
            PrintDebugMsg("Hit " + hit.transform.name);
            if (Vector3.Distance(transform.position, hit.transform.position) <= interactRange)
            {
                if (hit.transform.GetComponent<Ammo>() != null || hit.transform.GetComponent<FullAutoGun>() != null || hit.transform.GetComponent<OneShotReloadGun>() != null) pickupUI.gameObject.SetActive(true);
            }
        }
    }
    private void UpdateUI()
    {
        timerText.text = Timer.SINGLETON.GetTimeString(true);
        CheckPickupUI();
    }

    // Disable the info text telling the player the game's state after infoFadeTextDelay seconds.
    private IEnumerator FadeInfoText()
    {
        yield return new WaitForSeconds(infoTextFadeDelay);
        infoText.gameObject.SetActive(false);
    }

    // Exponential math problem.
    private float Exponent(float baseNum, float power)
    {
        float result = 1;
        for (int i = 0; i < power; i++) result *= baseNum;
        return result;
    }

    private void OnDeath()
    {
        respawnPanel.SetActive(true);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Camera Cam
    {
        get
        {
            if (cam == null) SetUpObjects();
            return cam;
        }
    }
    public int PlayerNum
    {
        set
        {
            playerNum = value;
        }
    }
    public bool IsPaused
    {
        set
        {
            isPaused = value;
        }
    }
    public float LookSpeed
    {
        get
        {
            return lookSpeed;
        }
        set
        {
            lookSpeed = value;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        SetUpObjects();
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        weapons = GetComponent<MonitorPlayerWeapons>();

        lastStep = Time.time;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {
        if (!grounded && GetComponent<Rigidbody>().velocity.y < 0 && GetComponent<Rigidbody>().velocity.y >= Physics.gravity.y)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.down * Mathf.Abs(Physics.gravity.y) * fallingSpeedMultiplier);
            fallingSpeedMultiplier++;
        }
    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        CheckGrounded();
        CheckInput();
        UpdateUI();
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}