﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebug = false;
    private string debugScriptName = "MainMenu";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private MainMenu otherCanvas = null;
    [SerializeField]
    private Text verText = null;
    [SerializeField]
    private GameObject matchSettingsPrefab = null;

    [SerializeField]
    private GameObject mainMenuPanel = null;
    [SerializeField]
    private Selectable selectableOnMMLoad = null;

    [SerializeField]
    private GameObject gameLobbyPanel = null;
    [SerializeField]
    private Selectable selectableOnLobbyLoad = null;
    [SerializeField]
    private Dropdown playersDropdown = null;
    [SerializeField]
    private Dropdown gameModeDropdown = null;
    [SerializeField]
    private Dropdown timeLimitDropdown = null;

    [SerializeField]
    private GameObject optionsPanel = null;
    [SerializeField]
    private Selectable selectableOnOptionsLoad = null;
    [SerializeField]
    private Slider p1SensitivitySlider = null;
    [SerializeField]
    private Slider p2SensitivitySlider = null;
    [SerializeField]
    private Slider p3SensitivitySlider = null;
    [SerializeField]
    private Slider p4SensitivitySlider = null;
    #endregion

    #region Private
    private MatchSettings matchSettingsObject = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // Load the one screen and hide the others. Then sync with other canvas.
    public void LoadMainMenu()
    {
        mainMenuPanel.SetActive(true);
        gameLobbyPanel.SetActive(false);
        if (optionsPanel != null) optionsPanel.SetActive(false);

        if (selectableOnMMLoad != null) selectableOnMMLoad.Select();

        if (optionsPanel != null)
        {
            float[] sensitivities = { p1SensitivitySlider.value, p2SensitivitySlider.value, p3SensitivitySlider.value, p4SensitivitySlider.value };
            matchSettingsObject.SetSensitivitySettings(sensitivities);
        }

        otherCanvas.SyncMainMenu();
    }
    public void SyncMainMenu()
    {
        mainMenuPanel.SetActive(true);
        gameLobbyPanel.SetActive(false);
        if (optionsPanel != null) optionsPanel.SetActive(false);
    }
    public void LoadGameLobby()
    {
        mainMenuPanel.SetActive(false);
        gameLobbyPanel.SetActive(true);
        if (optionsPanel != null) optionsPanel.SetActive(false);

        if (selectableOnLobbyLoad != null) selectableOnLobbyLoad.Select();

        otherCanvas.SyncGameLobby();
    }
    public void SyncGameLobby()
    {
        mainMenuPanel.SetActive(false);
        gameLobbyPanel.SetActive(true);
        if (optionsPanel != null) optionsPanel.SetActive(false);
    }
    public void LoadOptionsMenu()
    {
        mainMenuPanel.SetActive(false);
        gameLobbyPanel.SetActive(false);
        if(optionsPanel != null) optionsPanel.SetActive(true);

        if (selectableOnOptionsLoad != null) selectableOnOptionsLoad.Select();
    }

    // Save the settings to the Match settings object tell Unity not to remove while loading desired game mode.
    public void StartGame()
    {
        int numbOfPlayers = playersDropdown.value;
        float timeLimit = 0;
        switch(timeLimitDropdown.value)
        {
            case 0:
                timeLimit = 1;
                break;
            case 1:
                timeLimit = 1.5f;
                break;
            case 2:
                timeLimit = 2;
                break;
            case 3:
                timeLimit = 2.5f;
                break;
            case 4:
                timeLimit = 3;
                break;
        }
        matchSettingsObject.SetSettings(numbOfPlayers, timeLimit);
        DontDestroyOnLoad(matchSettingsObject.gameObject);

        switch(gameModeDropdown.value)
        {
            case 0:
                SceneManager.LoadScene("assassinGameMode");
                break;
            case 1:
                SceneManager.LoadScene("assaultGameMode");
                break;
        }
    }

    // Quit application
    public void Quit()
    {
        Application.Quit();
    } 

    // A dropdown was changed, sync dropdowns with other canvas.
    public void OnChangeSetting()
    {
        otherCanvas.SyncSettings(gameModeDropdown.value, playersDropdown.value, timeLimitDropdown.value);
    }
    // Called from other canvas when one of its dropdowns were changed.
    public void SyncSettings(int gameMode, int playerCount, int timeLimit)
    {
        gameModeDropdown.value = gameMode;
        playersDropdown.value = playerCount;
        timeLimitDropdown.value = timeLimit;
    }
    #endregion

    #region Private

    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters

    #endregion
    #endregion

    #region UnityFunctions

    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");

        if (matchSettingsPrefab != null)
        {
            GameObject obj = GameObject.Find("MatchSettings");
            if (obj == null)
            {
                matchSettingsObject = Instantiate(matchSettingsPrefab).GetComponent<MatchSettings>();
                matchSettingsObject.gameObject.name = "MatchSettings";
            }
            else matchSettingsObject = obj.GetComponent<MatchSettings>();

            p1SensitivitySlider.value = matchSettingsObject.P1LookSensitivity;
            p2SensitivitySlider.value = matchSettingsObject.P2LookSensitivity;
            p3SensitivitySlider.value = matchSettingsObject.P3LookSensitivity;
            p4SensitivitySlider.value = matchSettingsObject.P4LookSensitivity;
        }
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        LoadMainMenu();

        verText.text = "Version: " + Versions.SINGLETON.Version;
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
        if (gameModeDropdown.value == 0 && playersDropdown.value == 0) playersDropdown.value = 1;
    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}