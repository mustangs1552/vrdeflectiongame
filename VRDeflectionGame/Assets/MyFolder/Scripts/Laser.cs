﻿// (Unity3D) New monobehaviour script that includes regions for common sections, and supports debugging.
using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour
{
    #region GlobalVareables
    #region DefaultVareables
    public bool isDebugLaser = false;
    private string debugScriptName2 = "Laser";
    #endregion

    #region Static

    #endregion

    #region Public
    [SerializeField]
    private float force = 1;
    [SerializeField]
    private float damage = -5;
    #endregion

    #region Private
    private Team affiliation = Team.None;
    private Transform shooter = null;
    #endregion
    #endregion

    #region CustomFunction
    #region Static

    #endregion

    #region Public
    // When this laser is deflected by something then it swaps affiliation, looks at its shooter and moves toward them. The shooter is also replaced by the one who deflected it.
    public void Redirect(Transform newShooter)
    {
        PrintDebugMsg("Defelected!");

        if (newShooter == null) affiliation = Team.None;
        else if (affiliation == Team.MonitorPlayer) affiliation = Team.VRPlayer;
        else affiliation = Team.MonitorPlayer;

        transform.LookAt(shooter);
        shooter = newShooter;
        
        ApplyInitialForce();
    }
    #endregion

    #region Private
    // Apply the initial force to start moving.
    private void ApplyInitialForce()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().AddForce(transform.forward * force * 100);
    }
    #endregion

    #region Debug
    private void PrintDebugMsg(string msg)
    {
        if (isDebugLaser) Debug.Log(debugScriptName2 + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName2 + "(" + this.gameObject.name + "): " + msg);
    }
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName2 + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters_Setters
    public Team Affiliation
    {
        get
        {
            return affiliation;
        }
        set
        {
            affiliation = value;
        }
    }
    public Transform Shooter
    {
        get
        {
            return shooter;
        }
        set
        {
            shooter = value;
        }
    }
    public float Damage
    {
        get
        {
            return damage;
        }
    }
    #endregion
    #endregion

    #region UnityFunctions
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.GetComponent<Health>() == null && (collider.gameObject.GetComponent<PlasmaSword>() == null)) Destroy(gameObject);
    }
    #endregion

    #region Start_Update
    // Awake is called when the script instance is being loaded.
    void Awake()
    {
        PrintDebugMsg("Loaded.");
    }
    // Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    void Start()
    {
        Affiliation = Team.MonitorPlayer;

        ApplyInitialForce();
    }
    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    void FixedUpdate()
    {

    }
    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {

    }
    // LateUpdate is called every frame after all other update functions, if the Behaviour is enabled.
    void LateUpdate()
    {

    }
    #endregion
}